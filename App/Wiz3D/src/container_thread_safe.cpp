#include "container_thread_safe.h"
#include <QThread>
#include <QDebug>
ContainerThreadSafe::ContainerThreadSafe()
{
}

void ContainerThreadSafe::enqueue(const Sensor &sensor)
{
    QMutexLocker locker(&mutex);
    queue.enqueue(sensor);
}

Sensor ContainerThreadSafe::dequeue()
{
    QMutexLocker locker(&mutex);
    return queue.dequeue();
}

Sensor &ContainerThreadSafe::head()
{
    QMutexLocker locker(&mutex);
    return queue.head();
}

const Sensor &ContainerThreadSafe::head() const
{
    QMutexLocker locker(&mutex);
    return queue.head();
}

bool ContainerThreadSafe::isEmpty() const
{
    return queue.isEmpty();
}

int ContainerThreadSafe::length() const
{
    return queue.length();
}
