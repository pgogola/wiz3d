#include "object_view_widget.h"
#include "ui_object_view_widget.h"

#include <QDebug>
ObjectViewWidget::ObjectViewWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ObjectViewWidget)
{
    qDebug() << "OBJECT VIEW CREATED\n";
    ui->setupUi(this);
}

ObjectViewWidget::~ObjectViewWidget()
{
    delete ui;
}

MainSettingsWidget* ObjectViewWidget::getMainSettingsWidget() const
{
    return ui->mainSettingsWidget;
}

ObjectViewScene *ObjectViewWidget::getGlObjectWidget() const
{
    return ui->visualizationWidget;
}
