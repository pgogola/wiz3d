#include "main_window.h"
#include "ui_main_window.h"

#include <QDialog>
#include <QLabel>
#include <QSerialPortInfo>
#include <QDebug>
#include "container_thread_safe.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("3DWiz");

    gyroCalibrationOk = accCalibrationOk = false;

    /*!
    * \brief qRegisterMetaType<Sensor>
    * Register Sensor type. Needed when sending data from receiver to object model.
    * Otherwise an error occured:
    * "Cannot queue arguments of type .... "
    */
    qRegisterMetaType<Sensor>("Sensor");
    queue = new ContainerThreadSafe();

    objectModel = new ObjectModel(queue);

    objectModel->moveToThread(&objectModelThread);

    receiver = new Receiver(queue);
    receiver->moveToThread(&receiverThread);

    objectViewWidget = ui->mainAppWindow;
    menuBar = ui->menuBar;
    mainSettingsWidget = objectViewWidget->getMainSettingsWidget();
    objectViewScene3D = objectViewWidget->getGlObjectWidget();
    chartWindow = nullptr;

    initializeConnectionsModelView();
    menuAddAvailablePorts();

    mainSettingsWidget->setActive(false);

    receiverThread.start(QThread::Priority::HighestPriority);
    objectModelThread.start(QThread::Priority::HighestPriority);

    ui->actionZaladuj_model->setEnabled(false);
    ui->actionZa_aduj_tekstur->setEnabled(false);
}

MainWindow::~MainWindow()
{
    if(ui)
    {
        delete ui;
    }

    if(receiverThread.isRunning())
    {
        receiverThread.quit();
        receiverThread.wait();
    }

    if(receiver)
    {
        delete receiver;
    }

    if(objectModelThread.isRunning())
    {
        objectModelThread.quit();
        objectModelThread.wait();
    }

    if(objectModel)
    {
        delete objectModel;
    }

    if(queue)
    {
        delete queue;
    }

}

void MainWindow::menuAddAvailablePorts()
{
    QList<QSerialPortInfo> serialPorts = QSerialPortInfo::availablePorts();
    for(auto &x: serialPorts)
    {
        ui->menuWybierz_port->addAction(new QAction(x.portName(), this));
    }
    connect(ui->menuWybierz_port, &QMenu::triggered, this, &MainWindow::selectPortMenuOptions);
}

void MainWindow::startStop(bool isStart)
{
    if(isStart)
    {
        qDebug() << "Start receive data\n";
        mainSettingsWidget->setActive(receiver->startConnection());
    }
    else
    {
        qDebug() << "Stop receive data\n";
        receiver->stopConnection();
        mainSettingsWidget->setActive(false);
    }
}

void MainWindow::selectPortMenuOptions(QAction *action)
{
    receiver->setSerialPortName(action->iconText());
}

void MainWindow::openChartWindow(bool)
{
    chartWindow = new Charts(this);
    connect(objectModel, &ObjectModel::rotationVelocityChanged, chartWindow, &Charts::newRotation);
    connect(objectModel, &ObjectModel::translationAccelerationChanged, chartWindow, &Charts::newTranslation);

    connect(chartWindow, &Charts::destroyed,
            [=](){
        disconnect(objectModel, &ObjectModel::rotationVelocityChanged, chartWindow, &Charts::newRotation);
        disconnect(objectModel, &ObjectModel::rotationVelocityChanged, chartWindow, &Charts::newTranslation);
    });

    chartWindow->show();
}

void MainWindow::calibrationFinished(ObjectModel::SENSOR_ID sensor)
{
    static bool gyro = false;
    static bool acc = false;

    if(ObjectModel::SENSOR_ID::ACCELEROMETER == sensor)
    {
        acc = true;
    }
    else if(ObjectModel::SENSOR_ID::GYROSCOPE == sensor)
    {
        gyro = true;
    }
    if(acc && gyro)
    {
        disconnect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::sensorsCalibration);
        connect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::receiveNewData);
        gyro = false;
        acc = false;
        if(warningDialog)
            delete warningDialog;
        warningDialog = nullptr;
    }

}

void MainWindow::calibrationGyroFinished()
{
    gyroCalibrationOk = true;
    if(accCalibrationOk)
    {
        disconnect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::sensorsCalibration);
        connect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::receiveNewData);
        if(warningDialog)
        {
            warningDialog->hide();
            delete warningDialog;
        }
        warningDialog = nullptr;
        gyroCalibrationOk = accCalibrationOk = false;
    }
}

void MainWindow::calibrationAccFinished()
{
    accCalibrationOk = true;
    if(gyroCalibrationOk)
    {
        disconnect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::sensorsCalibration);
        connect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::receiveNewData);
        if(warningDialog)
        {
            warningDialog->hide();
            delete warningDialog;
        }
        warningDialog = nullptr;
        gyroCalibrationOk = accCalibrationOk = false;
    }
}

void MainWindow::startCalibration()
{
    if(receiver->getIsConnected())
    {
        disconnect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::receiveNewData);
        connect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::sensorsCalibration);

        warningDialog = new QMessageBox();
        warningDialog->setText(tr("Trwa kalibracja. Pozostaw urządzenie nieruchome."));
        warningDialog->setInformativeText(tr("Okno zostanie automatycznie zamknięte po zakończeniu kalibracji."));
        warningDialog->show();
    }
}

void MainWindow::initializeConnectionsModelView()
{
    /*!
     * \brief Connect events and slots which are result to object orientation change
     */
    connect(objectModel, &ObjectModel::orientationChanged,
            mainSettingsWidget, &MainSettingsWidget::setRotationValue);

    connect(objectModel, &ObjectModel::positionChanged,
            mainSettingsWidget, &MainSettingsWidget::setTranslationValue);

    /*!
     * \brief Start/stop object visualization
     */
    connect(mainSettingsWidget, &MainSettingsWidget::onStartStopChanged,
            this, &MainWindow::startStop);

    /*!
     * \brief Event to sensor calibration finished
     */
    connect(objectModel, &ObjectModel::calibrationGyroComplitted, this, &MainWindow::calibrationGyroFinished);
    connect(objectModel, &ObjectModel::calibrationAccComplitted, this, &MainWindow::calibrationAccFinished);

    connect(receiver, &Receiver::newSensorData, objectModel, &ObjectModel::sensorsCalibration);

    /*!
     * \brief Events from buttons click
     */
    connect(mainSettingsWidget, &MainSettingsWidget::onResetButtonReleased,
            [=](){objectModel->resetOrientation();
        objectModel->resetPosition();
        receiver->flushBuffer();});

    connect(mainSettingsWidget, &MainSettingsWidget::onCalibrationButtonReleased,
            this, &MainWindow::startCalibration);

    /*!
     * \brief Zoom in and out camera events
     */
    connect(mainSettingsWidget, &MainSettingsWidget::onZoomInButtonReleased,
            [=]()
    {
        objectViewScene3D->setCameraBy(QVector3D{0,0,1});
    });

    connect(mainSettingsWidget, &MainSettingsWidget::onZoomOutButtonReleased,
            [=]()
    {
        objectViewScene3D->setCameraBy(QVector3D{0,0,-1});
    });

    /*!
     * \brief Events from menu bar
     */
    connect(ui->actionStart, &QAction::triggered,
            [=](){mainSettingsWidget->setActive(receiver->startConnection());}
    );

    connect(ui->actionStop, &QAction::triggered,
            [=](){receiver->stopConnection();
        mainSettingsWidget->setActive(false);}
    );

    connect(ui->actionResetuj, &QAction::triggered,
            [=](){objectModel->resetOrientation();
        objectModel->resetPosition();});

    connect(ui->actionPoka_wykresy, &QAction::triggered,
            this, &MainWindow::openChartWindow);

    connect(ui->actionPrzybli, &QAction::triggered,
            [=]()
    {
        objectViewScene3D->setCameraBy(QVector3D{0,0,-1});
    });

    connect(ui->actionOddal, &QAction::triggered,
            [=]()
    {
        objectViewScene3D->setCameraBy(QVector3D{0,0,1});
    });

    connect(ui->actionZako_cz, &QAction::triggered,
            this, &MainWindow::close);

    connect(ui->actionO_programie, &QAction::triggered,
            [=]()
    {
        QMessageBox::information(this,
                                 tr("O programie"),
                                 tr("To jest program\n"));
    });

    connect(ui->actionManipulator, &QAction::triggered,
            [=]()
    {
        objectViewScene3D->setVisualizationMode(ObjectViewScene::VisualizationMode::MANIPULATOR);
        ui->actionZaladuj_model->setEnabled(false);
        ui->actionZa_aduj_tekstur->setEnabled(false);
    });

    connect(ui->actionGra, &QAction::triggered,
            [=]()
    {
        objectViewScene3D->setVisualizationMode(ObjectViewScene::VisualizationMode::GAME);
        ui->actionZaladuj_model->setEnabled(false);
        ui->actionZa_aduj_tekstur->setEnabled(false);
    });

    connect(ui->actionWizualizacja_obiektu, &QAction::triggered,
            [=]()
    {
        objectViewScene3D->setVisualizationMode(ObjectViewScene::VisualizationMode::OBJECT_VISUALIZATION);
        ui->actionZaladuj_model->setEnabled(true);
        ui->actionZa_aduj_tekstur->setEnabled(true);
    });

    connect(ui->actionZaladuj_model, &QAction::triggered,
            [=]()
    {
        QUrl objFileUrl =
                QFileDialog::getOpenFileUrl(this,
                                            tr("Wybór modelu do załadowania"),
                                            QUrl(),
                                            "Model 3D (*.obj)");
        qDebug() << objFileUrl.toString();
        if(0!=objFileUrl.toString().length())
        {
            objectViewScene3D->setObjectModelSource(objFileUrl);
        }
    });

    connect(ui->actionZa_aduj_tekstur, &QAction::triggered,
            [=]()
    {
        QUrl objFileUrl =
                QFileDialog::getOpenFileUrl(this,
                                            tr("Wybór tekstury do załadowania"),
                                            QUrl(),
                                            "Plik tekstury (*.webp)");
        qDebug() << objFileUrl.toString();
        if(0!=objFileUrl.toString().length())
        {
            objectViewScene3D->setObjectTextureSource(objFileUrl);
        }
    });

    connect(objectModel, &ObjectModel::orientationChanged, objectViewScene3D, &ObjectViewScene::setOrientation);
    connect(objectModel, &ObjectModel::positionChanged, objectViewScene3D, &ObjectViewScene::setPosition);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, tr("Wiz3D"),
                                                                tr("Czy zamknąć aplikację?\n"),
                                                                QMessageBox::Cancel |
                                                                QMessageBox::No |
                                                                QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}
