#include "main_window.h"
#include <QApplication>
#include <QDebug>
#include "receiver.h"
#include "object_model.h"

/*!
 * \mainpage Wiz3D - 3D object visualization
 *
 * \author Piotr Gogola
 * \date 11.05.1018
 * \section etykieta-informacje Informacje o projekcie
 * Aplikacja zrealizowana w ramach projektu na kursu "Wizualizacja danych sensorycznych".<BR>
 * Politechnika Wrocławska<BR>
 * Semestr letni 2017/18 <BR>
 * Prowadzący: dr inż. Bogdan Kreczmer
 *
 * \section etykieta-opis Opis aplikacji
 * Aplikacja napisana z wykorzystaniem biblioteki Qt w wersji 5.9.1.
 * <BR>
 * Program umożliwia wizualizację obiektów 3D. Kontrolowanie orientacji oraz położenia obiektu
 * odbywa się za pomocą danych otrzymanych z żyroskopu oraz akcelerometru znajdujących się na płytce
 * deweloperskiej STM32L476-Discovery. Komunikacja z płytką odbywa się za pomocą interfejsu UART.
 *
 * Aplikacja udostępnia wizualizację otrzymanych danych sensorycznych w postaci wykresów otwieranych
 * w dodatkowym oknie.
 *
 * Program może pracować w dwóch trybach:
 * - wizualizacja prostego manipulatora o 3 stopniach swobody. Dane o orientacji płytki STM32L476
 * są przetwarzane i wizualizowane w postaci kątów w przegubach manipulatora,
 * - wizualizacja dowolnego obiektu 3D załadowanego z pliku obj. Orientacja i pozycja płytki
 * deweloperskiej przekłada się na orientację i pozycję obiektu na ekranie komputera.
*/


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("3DWiz");
    MainWindow w;
    w.show();
    return a.exec();
}
