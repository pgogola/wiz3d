#include "game.h"

#include <QColor>
#include <QRgb>

Game::Game(Qt3DCore::QNode *rootEntity, QObject *parent)
    : QObject(parent),
      parentEntity(rootEntity),
      mainObject(new CollisionObject(0.02f,
                                     QVector3D(0,0,0),
                                     QColor(QRgb(0x0000AA)),
                                     objSize,
                                     parentEntity,
                                     QUrl(QStringLiteral("qrc:/Starship.obj")))),
      obstaclesList(new QList<CollisionObject*>()),
      pointsList(new QList<CollisionObject*>()),
      timer(new QTimer()),
      randGen(new std::uniform_int_distribution<int>(-50, 50))
{
    mainObject->transform()->setRotationY(180);
}

Game::~Game()
{
    for(auto x: *obstaclesList)
    {
        delete x;
    }
    for(auto x: *pointsList)
    {
        delete x;
    }
}

void Game::start()
{
    collisionCounter = 0;
    pointsCounter = 0;
    connect(timer.get(), &QTimer::timeout, this, &Game::update);
    timer->start(50);
}

void Game::stop()
{
    disconnect(timer.get(), &QTimer::timeout, this, &Game::update);
    timer->stop();
}

void Game::setMainObjectPosition(const QVector3D &position)
{
    mainObject->transform()->setTranslation(QVector3D(position.x(), 0, 0));
}

void Game::update()
{
    /*!
     * Check collisions with all asteroids
     */
    for(auto i = 0; i<obstaclesList->length(); i++)
    {
        (*obstaclesList)[i]->moveBy(QVector3D(0,0,-3));
        (*obstaclesList)[i]->transform()->setRotationY((*obstaclesList)[i]->transform()->rotationY()-10);
        if(mainObject->isCollision(*obstaclesList->at(i)))
        {
            delete (*obstaclesList)[i];
            obstaclesList->removeAt(i);
            i--;
            collisionCounter++;
            emit newCollision();
        }
        else if((*obstaclesList)[i]->transform()->translation().z() > mainObject->transform()->translation().z()+20)
        {
            delete (*obstaclesList)[i];
            obstaclesList->removeAt(i);
            i--;
        }
    }

    /*!
     * Check collisions with stars
     */
    for(auto i = 0; i<pointsList->length(); i++)
    {
        (*pointsList)[i]->moveBy(QVector3D(0,0,-3));
        (*pointsList)[i]->transform()->setRotationY((*pointsList)[i]->transform()->rotationY()-10);
        if(mainObject->isCollision(*pointsList->at(i)))
        {
            delete (*pointsList)[i];
            pointsList->removeAt(i);
            i--;
            pointsCounter++;
            emit newPoint();
        }
        else if((*pointsList)[i]->transform()->translation().z() > mainObject->transform()->translation().z()+20)
        {
            delete (*pointsList)[i];
            pointsList->removeAt(i);
            i--;
        }
    }

    /*!
     * Add asteroid to the scene
     */
    if(randGen->operator()(engine)<-10)
    {
        obstaclesList->append(new CollisionObject(0.05f, QVector3D(randGen->operator()(engine),0,-150), /*Qt::gray*/QColor(QRgb(0x4d4d33)), 3.0f, parentEntity, QUrl(QStringLiteral("qrc:/asteroida.obj"))));
    }
    /*!
     * Add star to the scene
     */
    if(randGen->operator()(engine)<-10)
    {
        CollisionObject *obj = new CollisionObject(0.2f, QVector3D(randGen->operator()(engine),0,-150), Qt::yellow, objSize, parentEntity, QUrl(QStringLiteral("qrc:/estrellica.obj")));
        obj->transform()->setRotationX(90);
        pointsList->append(obj);
    }

}

int Game::getCollisionCounter() const
{
    return collisionCounter;
}

int Game::getPointsCounter() const
{
    return pointsCounter;
}

bool Game::isActive()
{
    return timer->isActive();
}
