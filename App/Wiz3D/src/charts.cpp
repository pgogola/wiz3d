#include "charts.h"
#include "ui_charts.h"
#include <QDebug>
Charts::Charts(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Charts)
{
    ui->setupUi(this);
    initializeCharts();
    setWindowTitle(tr("Wykresy"));
}

void Charts::initializeChart(QCustomPlot* chart, const QString& xLabel,
                             const QString& yLabel, QPointF xRange, QPointF yRange)
{
    chart->addGraph();
    chart->graph()->setPen(QPen(Qt::red, 1));
    /**
     * Labels
     */
    chart->xAxis->setLabel(xLabel);
    chart->yAxis->setLabel(yLabel);
    // set axes ranges, so we see all data:
    chart->xAxis->setRange(xRange.x(), xRange.y());
    chart->yAxis->setRange(yRange.x(), yRange.y());
    chart->replot();
    chart->setNoAntialiasingOnDrag(true);
}

void Charts::initializeCharts()
{
    initializeChart(ui->xRotation, "time", "Rot(x, a)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ROT_VAL, MAX_Y_AXIS_ROT_VAL));
    initializeChart(ui->yRotation, "time", "Rot(y, B)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ROT_VAL, MAX_Y_AXIS_ROT_VAL));
    initializeChart(ui->zRotation, "time", "Rot(z, n)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ROT_VAL, MAX_Y_AXIS_ROT_VAL));

    initializeChart(ui->xTranslation, "time", "Tran(x, a)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ACC_VAL, MAX_Y_AXIS_ACC_VAL));
    initializeChart(ui->yTranslation, "time", "Tran(y, B)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ACC_VAL, MAX_Y_AXIS_ACC_VAL));
    initializeChart(ui->zTranslation, "time", "Tran(z, n)", QPointF(0, MAX_TIME_PERIOD), QPointF(-MAX_Y_AXIS_ACC_VAL, MAX_Y_AXIS_ACC_VAL));

}

Charts::~Charts()
{
    delete ui;
}

void Charts::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::Yes;
    resBtn = QMessageBox::question( this, tr("Wykresy"),
                                    tr("Czy zamknąć okno wykresów?\n"
                                       "Dotychczasowe wykresy zostaną utracone.\n"),
                                    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                    QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}

void Charts::addAndReplot(QCustomPlot* plot, const double x, const double y)
{
    plot->graph(0)->data()->mData.append(QCPGraphData(x,y));

    if( plot->graph(0)->data()->mData.last().key
            - plot->graph(0)->data()->mData.first().key > MAX_TIME_PERIOD)
    {
        plot->graph(0)->data()->mData.removeFirst();
        plot->xAxis->setRange(plot->graph(0)->data()->mData.first().key,
                              plot->graph(0)->data()->mData.last().key);
    }
    plot->replot();
    plot->update();
}

void Charts::newRotation(const QVector3D &rotation)
{
    static int i = 0;
    // draw each fifth data becouse of performance
    if(i<5)
    {
        i++;
        return;
    }
    static QTime prev = QTime::currentTime();
    float dt = (float)prev.msecsTo(QTime::currentTime())/1000.0;
    addAndReplot(ui->xRotation, dt, rotation.x());
    addAndReplot(ui->yRotation, dt, rotation.y());
    addAndReplot(ui->zRotation, dt, rotation.z());
    i=0;
}

void Charts::newTranslation(const QVector3D &translation)
{
    static int i = 0;
    // draw each fifth data becouse of performance
    if(i<5)
    {
        i++;
        return;
    }
    static QTime prev = QTime::currentTime();
    float dt = (float)prev.msecsTo(QTime::currentTime())/1000.0;
    addAndReplot(ui->xTranslation, dt, translation.x());
    addAndReplot(ui->yTranslation, dt, translation.y());
    addAndReplot(ui->zTranslation, dt, translation.z());
    i=0;
}
