#include "object_view_scene.h"

#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/Qt3Dwindow.h>
#include <Qt3DCore/QEntity>
#include <QCamera>
#include <QFirstPersonCameraController>
#include <QInputAspect>
#include <QPointLight>
#include <QTransform>
#include <QScreen>
#include <QLayout>
#include <manipulator.h>
#include <QDiffuseMapMaterial>
#include <QTextureImage>
#include <QCheckBox>
#include <QComponent>
#include <game.h>
#include <QPushButton>
#include <QLabel>
#include <Qt3DExtras/QForwardRenderer>


ObjectViewScene::ObjectViewScene(QWidget *parent)
    : QWidget(parent), manip(nullptr), objectEntity(nullptr),
      game(nullptr), moveCheckBox(nullptr)
{
    Qt3DExtras::Qt3DWindow *view = new Qt3DExtras::Qt3DWindow();
    view->defaultFrameGraph()->setClearColor(QColor(QRgb(0x4d4d4f)));
    QWidget *container = QWidget::createWindowContainer(view);
    QSize screenSize = view->screen()->size();
    container->setMinimumSize(QSize(200, 100));
    container->setMaximumSize(screenSize);

    Qt3DInput::QInputAspect *input = new Qt3DInput::QInputAspect;
    view->registerAspect(input);

    // Root entity
    rootEntity = new Qt3DCore::QEntity();

    // Camera
    cameraEntity = view->camera();

    cameraEntity->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    cameraEntity->setPosition(QVector3D(0, 20.0f, 60.0f));
    cameraEntity->setUpVector(QVector3D(0, 1, 0));
    cameraEntity->setViewCenter(QVector3D(0, 0, 0));

    Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(rootEntity);
    Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(1);
    lightEntity->addComponent(light);
    Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(cameraEntity->position());
    lightEntity->addComponent(lightTransform);

    // For camera controls
    camController = new Qt3DExtras::QFirstPersonCameraController(rootEntity);
    camController->setCamera(cameraEntity);

    // Visualization mode init
    visualizationMode = VisualizationMode::MANIPULATOR;

    // Create manipulator
    manip = new Manipulator(rootEntity);
    manip->createManipulator();

    // Set root object of the scene
    view->setRootEntity(rootEntity);

    this->setLayout(new QGridLayout);

    moveCheckBox= new QCheckBox(this);
    moveCheckBox->setText("Przemieszczenie obiektu");
    QPalette plt;
    plt.setColor(QPalette::WindowText, Qt::red);
    moveCheckBox->setPalette(plt);
    moveCheckBox->hide();
    moveCheckBox->setEnabled(false);
    moveCheckBox->setChecked(false);
    layout()->addWidget(moveCheckBox);
    container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout()->addWidget(container);
    objectModelEntityUrl = QStringLiteral("qrc:/Chest.obj");
    objectTextureUrl = QStringLiteral("qrc:/diffuse.webp");


    gameButton = new QPushButton("Graj", this);
    greenPointsLabel = new QLabel("---", this);
    greenPointsLabel->setAlignment(Qt::AlignCenter);
    plt.setColor(QPalette::WindowText, QColor(QRgb(0x009933)));
    greenPointsLabel->setPalette(plt);
    redPointsLabel = new QLabel("---", this);
    redPointsLabel->setAlignment(Qt::AlignCenter);
    plt.setColor(QPalette::WindowText, Qt::red);
    redPointsLabel->setPalette(plt);
    showGameStatus(false);
    QHBoxLayout *hbx = new QHBoxLayout(this);
    hbx->addWidget(gameButton);
    hbx->addWidget(greenPointsLabel);
    hbx->addWidget(redPointsLabel);
    layout()->addItem(hbx);
    showGameStatus(false);
}

void ObjectViewScene::setObjectModelSource(QUrl source)
{
    objectModelEntityUrl = source;
    createRenderableObject();
}

void ObjectViewScene::setObjectTextureSource(QUrl source)
{
    objectTextureUrl = source;
    if(!objectDiffuseImage)
    {
        objectDiffuseImage = new Qt3DRender::QTextureImage();
    }
    objectDiffuseImage->setSource(objectTextureUrl);
    diffuseMapMaterial->diffuse()->addTextureImage(objectDiffuseImage);
}

void ObjectViewScene::setOrientation(const QQuaternion &orientation)
{
    this->orientation = orientation;
    if(VisualizationMode::MANIPULATOR == visualizationMode)
    {
        QVector3D rot = this->orientation.toEulerAngles();
        manip->setQ1(rot.y());
        manip->setQ2(rot.z()+90);
        manip->setQ3(rot.x()+90);
    }
    else if(VisualizationMode::OBJECT_VISUALIZATION == visualizationMode)
    {
        objectEntity->transform()->setRotation(this->orientation);
    }
}

void ObjectViewScene::setPosition(const QVector3D &value)
{
    this->position = value;
    if(VisualizationMode::OBJECT_VISUALIZATION == visualizationMode
            && moveCheckBox->isChecked())
    {
        objectEntity->transform()->setTranslation(2*this->position);
    }
    else if(VisualizationMode::GAME == visualizationMode)
    {
        game->setMainObjectPosition(2*this->position);
    }
}

void ObjectViewScene::setCamera(const QVector3D &value)
{
    cameraEntity->setPosition(cameraEntity->position()-value);
}

void ObjectViewScene::setCameraBy(const QVector3D &value)
{
    cameraEntity->setPosition(cameraEntity->position()-value);
}

void ObjectViewScene::setVisualizationMode(ObjectViewScene::VisualizationMode mode)
{
    if(visualizationMode == mode)
    {
        return;
    }
    if(manip)
    {
        delete manip;
        manip = nullptr;
        moveCheckBox->hide();
        moveCheckBox->setEnabled(false);
    }
    if(objectEntity)
    {
        delete objectEntity;
        objectEntity = nullptr;
    }
    if(game)
    {
        game->stop();
        disconnect(gameConnection);
        delete game;
        game = nullptr;
        showGameStatus(false);
        gameButton->setText(tr("Start"));
    }
    visualizationMode = mode;
    switch(visualizationMode)
    {
    case VisualizationMode::MANIPULATOR:

        manip = new Manipulator(rootEntity);
        manip->createManipulator();
        break;

    case VisualizationMode::OBJECT_VISUALIZATION:

        createRenderableObject();
        moveCheckBox->show();
        moveCheckBox->setEnabled(true);
        break;

    case VisualizationMode::GAME:
        game = new Game(rootEntity);
        connect(game, &Game::newCollision, [&](){redPointsLabel->setText("Liczba kolizji: " + QString::number(game->getCollisionCounter()));});
        connect(game, &Game::newPoint, [&](){greenPointsLabel->setText("Zdobyte punkty: " + QString::number(game->getPointsCounter()));});
        gameConnection = QObject::connect(gameButton, &QPushButton::released, [&](){
            if(game->isActive())
            {
                game->stop();
                gameButton->setText(tr("Start"));
            }
            else
            {
                game->start();
                gameButton->setText(tr("Stop"));
            }
        });
        showGameStatus(true);
        break;
    }
}

void ObjectViewScene::createRenderableObject()
{
    if(objectEntity)
    {
        delete objectEntity;
    }
    objectEntity = new RenderableEntity(rootEntity);
    objectEntity->transform()->setScale(0.25f);
    objectEntity->mesh()->setSource(objectModelEntityUrl);
    diffuseMapMaterial = new Qt3DExtras::QDiffuseMapMaterial();
    diffuseMapMaterial->setSpecular(QColor::fromRgbF(0.2f, 0.2f, 0.2f, 1.0f));
    diffuseMapMaterial->setShininess(2.0f);
    if(0!=objectTextureUrl.toString().length())
    {
        objectDiffuseImage = new Qt3DRender::QTextureImage();
        objectDiffuseImage->setSource(objectTextureUrl);
        diffuseMapMaterial->diffuse()->addTextureImage(objectDiffuseImage);
    }
    else
    {
        objectDiffuseImage = nullptr;
    }
    objectEntity->addComponent(diffuseMapMaterial);
}

