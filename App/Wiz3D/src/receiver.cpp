/*!
 * \file
 * \brief Definition of Receiver class methods.
 */

#include "receiver.h"
#include <QSerialPortInfo>
#include <QDebug>
#include <QLabel>
#include <iostream>
#include <cstdint>
#include "crc16.h"
#include <fstream>
#include <QThread>

Receiver::Receiver(ContainerThreadSafe *q, QObject *parent)
    : QObject(parent), buffer(new QByteArray), isConnected(false), queue(q)
{
    serialPort.setBaudRate(QSerialPort::Baud115200);
    serialPort.setDataBits(QSerialPort::Data8);
}

Receiver::~Receiver()
{
    if(serialPort.isOpen())
    {
        stopConnection();
    }
    if(buffer)
    {
        delete buffer;
    }
}

bool Receiver::startConnection()
{
    serialPort.setPortName(serialPortName);
    isConnected = serialPort.open(QIODevice::ReadWrite);
    if(isConnected)
    {
        connect(&serialPort, &QSerialPort::readyRead, this, &Receiver::receiveData);
    }
    return isConnected;
}

void Receiver::stopConnection()
{
    if(isConnected)
    {
        disconnect(&serialPort, &QSerialPort::readyRead, this, &Receiver::receiveData);
        serialPort.close();
        isConnected = false;
    }
}

void Receiver::receiveData()
{
    //    qDebug() << "void Receiver::receiveData() thread id = " << QThread::currentThreadId() << "\n";
    buffer->append(serialPort.readAll());
    int start = 0;
    int len;

    /*!
      * Find start of frame, check if after start of frame there is
      * another byte to read length
      */
    if(-1 == (start = buffer->indexOf(0xaa))
            || buffer->length()<=(start+1))
    {
        return;
    }

    len = buffer->at(start+1);
    /**
      * Check if can read last byte if frame
      */
    if(buffer->length()-start < len)
    {
        return;
    }

    /**
     * Read last byte in frame, check end of frame is correct
     */
    if(buffer->at(start+len-1) != 0x55)
    {
        buffer->remove(0, start+len-1);
        return;
    }

    /**
     * Get frame
     */
    uint8_t* payload = (uint8_t*)(buffer->data()+start);

    /**
     * Get CRC, check sum
     */
    uint16_t crc = (payload[len-3] << 8) | (payload[len-2]);
    if(crc_16(payload+1, len-4) == crc)
    {
        sensor = (Sensor*)(payload+2);
//        printData();
        queue->enqueue(*sensor);
        emit newSensorData();
    }
    buffer->remove(0, start+len);
    qDebug() << "size of buffer " << buffer->size();
}

bool Receiver::getIsConnected() const
{
    return isConnected;
}

void Receiver::printData()
{
    qDebug() << "sensor " << sensor->getSensorID() << " " << sensor->getX() << " " << sensor->getY() <<" " << sensor->getZ() << "\n";
}

const QString &Receiver::getSerialPortName() const
{
    return serialPortName;
}

void Receiver::setSerialPortName(const QString &value)
{
    serialPortName = value;
}

void Receiver::flushBuffer()
{
    buffer->clear();
}
