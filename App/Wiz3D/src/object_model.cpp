/*!
 * \file
 * \brief Definition of ObjectModel class
 *
 *
 */

#include "object_model.h"
#include <memory>
#include <QDebug>
#include <QTime>
#include <QQuaternion>
#include <QTransform>
#include <qtransform.h>
#include <QThread>
#include <QLinkedList>

ObjectModel::ObjectModel(ContainerThreadSafe *q, QObject *parent)
    : QObject(parent), queue(q)
{
}

ObjectModel::~ObjectModel()
{

}

void ObjectModel::calculateRotationVelocity(const Sensor sensor)
{
    /*!
     * \brief Integrate gyroscope self gain.
     * If gyroscope data in axes are betwen min and max then result is equal zero.
     * Otherwise use mean value for corrention.
     */
    int X = sensor.getX(), Y=sensor.getY(), Z=sensor.getZ();
    if(sensor.getX() >= xGyroscopeMin && sensor.getX() <= xGyroscopeMax)
    {
        X = 0;
    }
    else
    {
        X = sensor.getX() - xGyroscopeMean;
    }

    if(sensor.getY() >= yGyroscopeMin && sensor.getY() <= yGyroscopeMax)
    {
        Y = 0;
    }
    else
    {
        Y = sensor.getY() - yGyroscopeMean;
    }

    if(sensor.getZ() >= zGyroscopeMin && sensor.getZ() <= zGyroscopeMax)
    {
        Z = 0;
    }
    else
    {
        Z = sensor.getZ() - zGyroscopeMean;
    }
    /*!
     *  Swift axes becouse axes in GL are not the same as in gyroscope
     */
    rotationVelocity.setX(X*GYROSCOPE_SENSITIV);
    rotationVelocity.setY(Y*GYROSCOPE_SENSITIV);
    rotationVelocity.setZ(Z*GYROSCOPE_SENSITIV);

    calculateOrientation();
    emit rotationVelocityChanged(rotationVelocity);
}

void ObjectModel::calculateOrientation()
{
    static QTime prev = QTime::currentTime();
    float dt = (float)prev.msecsTo(QTime::currentTime())/1000.0;
    prev = QTime::currentTime();

    orientation *= QQuaternion::fromEulerAngles(rotationVelocity.x()*dt,
                                                rotationVelocity.y()*dt,
                                                rotationVelocity.z()*dt);
    emit orientationChanged(orientation);
}

void ObjectModel::calculateTranslationAcceleration(const Sensor sensor)
{
    /*
     * Variables for high pass filter
     */
    static int gravX=0, gravY=0, gravZ=0;
    /*!
     * \brief Integrate accelerometer self gain.
     * If accelerometer data in axes are betwen min and max then result is equal zero.
     * Otherwise use mean value for corrention.
     */
    int X = sensor.getX(), Y=sensor.getY(), Z=sensor.getZ();
//    rawAccelerometerData = QVector3D(sensor.getX(), sensor.getY(), sensor.getZ());


    /*
     * Filter data, remove gravity acceleration
     */
    float alpha = 0.09;
    gravX = gravX*alpha + X*(1-alpha);
    gravY = gravY*alpha + Y*(1-alpha);
    gravZ = gravZ*alpha + Z*(1-alpha);


    translationAcceleration.setX(rawToMps(X-gravX));
    translationAcceleration.setY(rawToMps(Y-gravY));
    translationAcceleration.setZ(rawToMps(Z-gravZ));

    calculatePosition();
    emit translationAccelerationChanged(translationAcceleration);
}

void ObjectModel::calculatePosition()
{
    static QTime prev = QTime::currentTime();
    float dt = (float)prev.msecsTo(QTime::currentTime())/1000.0;
    prev = QTime::currentTime();
    position.setX(position.x() + translationVelocity.x()*dt + 0.5*translationAcceleration.x()*dt*dt);
    position.setY(position.y() + translationVelocity.y()*dt + 0.5*translationAcceleration.y()*dt*dt);
    position.setZ(position.z() + translationVelocity.z()*dt + 0.5*translationAcceleration.z()*dt*dt);

    translationVelocity.setX(translationVelocity.x()*0.90 + translationAcceleration.x()*dt);
    translationVelocity.setY(translationVelocity.y()*0.90  + translationAcceleration.y()*dt);
    translationVelocity.setZ(translationVelocity.z()*0.90  + translationAcceleration.z()*dt);
    emit positionChanged(2000*position);
}

void ObjectModel::receiveNewData()
{
    while(queue->isEmpty())
    {
        QThread::currentThread()->msleep(10);
    }
    Sensor sensor = queue->dequeue();
    switch (sensor.getSensorID()) {
    case SENSOR_ID::GYROSCOPE:
        calculateRotationVelocity(sensor);
        break;
    case SENSOR_ID::ACCELEROMETER:
        calculateTranslationAcceleration(sensor);
        break;
    default:
        break;
    };
}

void ObjectModel::sensorsCalibration()
{
    while(queue->isEmpty())
    {
        QThread::currentThread()->msleep(10);
    }
    Sensor sensor = queue->dequeue();
    switch (sensor.getSensorID()) {
    case SENSOR_ID::GYROSCOPE:
        gyroscopeCalibration(sensor);
        break;
    case SENSOR_ID::ACCELEROMETER:
        accelerometerCalibration(sensor);
        break;
    default:
        break;
    };
}

void ObjectModel::gyroscopeCalibration(const Sensor sensor)
{
    static constexpr int MAX_SAMPLES_AMOUNT = 100;
    static int sampleNumber = 0;

    if(0 == sampleNumber)
    {
        xGyroscopeMax = INT_MIN;
        xGyroscopeMin = INT_MAX;
        xGyroscopeMean = 0;
        yGyroscopeMax = INT_MIN;
        yGyroscopeMin = INT_MAX;
        yGyroscopeMean = 0;
        zGyroscopeMax = INT_MIN;
        zGyroscopeMin = INT_MAX;
        zGyroscopeMean = 0;
    }

    xGyroscopeMax = qMax(xGyroscopeMax, int(sensor.getX()));
    yGyroscopeMax = qMax(yGyroscopeMax, int(sensor.getY()));
    zGyroscopeMax = qMax(zGyroscopeMax, int(sensor.getZ()));

    xGyroscopeMin = qMin(xGyroscopeMin, int(sensor.getX()));
    yGyroscopeMin = qMin(yGyroscopeMin, int(sensor.getY()));
    zGyroscopeMin = qMin(zGyroscopeMin, int(sensor.getZ()));

    xGyroscopeMean += sensor.getX();
    yGyroscopeMean += sensor.getY();
    zGyroscopeMean += sensor.getZ();

    sampleNumber++;
    if(MAX_SAMPLES_AMOUNT <= sampleNumber)
    {
        xGyroscopeMean/=MAX_SAMPLES_AMOUNT;
        yGyroscopeMean/=MAX_SAMPLES_AMOUNT;
        zGyroscopeMean/=MAX_SAMPLES_AMOUNT;
        sampleNumber = 0;
        emit calibrationGyroComplitted();
    }
}

void ObjectModel::accelerometerCalibration(const Sensor sensor)
{
    static constexpr int MAX_SAMPLES_AMOUNT = 100;
    static int sampleNumber = 0;

    if(0 == sampleNumber)
    {
        xAccelerometerMax = INT_MIN;
        xAccelerometerMin = INT_MAX;
        xAccelerometerMean = 0;
        yAccelerometerMax = INT_MIN;
        yAccelerometerMin = INT_MAX;
        yAccelerometerMean = 0;
        zAccelerometerMax = INT_MIN;
        zAccelerometerMin = INT_MAX;
        zAccelerometerMean = 0;
    }

    xAccelerometerMax = qMax(xAccelerometerMax, int(sensor.getX()));
    yAccelerometerMax = qMax(yAccelerometerMax, int(sensor.getY()));
    zAccelerometerMax = qMax(zAccelerometerMax, int(sensor.getZ()));

    xAccelerometerMin = qMin(xAccelerometerMin, int(sensor.getX()));
    yAccelerometerMin = qMin(yAccelerometerMin, int(sensor.getY()));
    zAccelerometerMin = qMin(zAccelerometerMin, int(sensor.getZ()));

    xAccelerometerMean += sensor.getX();
    yAccelerometerMean += sensor.getY();
    zAccelerometerMean += sensor.getZ();

    sampleNumber++;
    if(MAX_SAMPLES_AMOUNT <= sampleNumber)
    {
        xAccelerometerMean/=MAX_SAMPLES_AMOUNT;
        yAccelerometerMean/=MAX_SAMPLES_AMOUNT;
        zAccelerometerMean/=MAX_SAMPLES_AMOUNT;
        sampleNumber = 0;
        emit calibrationAccComplitted();
    }
}

const QQuaternion& ObjectModel::getOrientation() const
{
    return orientation;
}

void ObjectModel::setOrientation(const QQuaternion &value)
{
    orientation = value;
    emit orientationChanged(orientation);
}

const QVector3D& ObjectModel::getPosition() const
{
    return position;
}

void ObjectModel::setPosition(const QVector3D &value)
{
    position = value;
    emit positionChanged(position);
}

const QVector3D& ObjectModel::getTranslationAcceleration() const
{
    return translationAcceleration;
}

void ObjectModel::setTranslationAcceleration(const QVector3D &value)
{
    translationAcceleration = value;
}

const QVector3D& ObjectModel::getRotationVelocity() const
{
    return rotationVelocity;
}

void ObjectModel::setRotationVelocity(const QVector3D &value)
{
    rotationVelocity = value;
}

void ObjectModel::resetOrientation()
{
    orientation = QQuaternion();
    emit orientationChanged(orientation);
}

void ObjectModel::resetPosition()
{
    position = QVector3D();
    translationVelocity = QVector3D();
    emit positionChanged(position);
}

const QVector3D& ObjectModel::getTranslationVelocity() const
{
    return translationVelocity;
}

void ObjectModel::setTranslationVelocity(const QVector3D &value)
{
    translationVelocity = value;
}
