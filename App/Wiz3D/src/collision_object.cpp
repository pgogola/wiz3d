#include "collision_object.h"

#include <QCuboidMesh>
#include <QPhongMaterial>
#include <QGeometry>
CollisionObject::CollisionObject(const float& scale,
                                 const QVector3D& location,
                                 const QColor& color,
                                 const float size,
                                 Qt3DCore::QNode *parent,
                                 const QUrl &modelURL)
    : RenderableEntity(parent), objSize(size)
{
    Qt3DExtras::QPhongMaterial *cuboidMaterial = new Qt3DExtras::QPhongMaterial();
    cuboidMaterial->setDiffuse(color);

    //    transform()->setScale3D(dimensions);
    transform()->setScale(scale);
    transform()->setTranslation(location);

    if(modelURL.isEmpty())
    {
        Qt3DExtras::QCuboidMesh *cuboid = new Qt3DExtras::QCuboidMesh();
        addComponent(cuboid);
    }
    else
    {
        mesh()->setSource(modelURL);
    }
    addComponent(cuboidMaterial);
}

bool CollisionObject::isCollision(const CollisionObject& other)
{
        if((transform()->translation()-other.transform()->translation()).length() < other.objSize+objSize)
    {
        emit collisionOccured();
        return true;
    }
    return false;
}

void CollisionObject::moveBy(const QVector3D &movement)
{
    transform()->setTranslation(transform()->translation()-movement);
}

void CollisionObject::setTranslation(const QVector3D &tra)
{
    transform()->setTranslation(tra);
}
