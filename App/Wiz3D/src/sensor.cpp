#include "sensor.h"

Sensor::Sensor()
{

}

qint16 Sensor::getX() const
{
    return x;
}

void Sensor::setX(const qint16 &value)
{
    x = value;
}

qint16 Sensor::getY() const
{
    return y;
}

void Sensor::setY(const qint16 &value)
{
    y = value;
}

qint16 Sensor::getZ() const
{
    return z;
}

void Sensor::setZ(const qint16 &value)
{
    z = value;
}

quint16 Sensor::getSensorID() const
{
    return sensorID;
}
