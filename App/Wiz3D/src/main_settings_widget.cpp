#include "main_settings_widget.h"
#include "ui_main_settings_widget.h"
#include <QDebug>
#include <QPainter>
#include <QQuaternion>
#include <QTranslator>



MainSettingsWidget::MainSettingsWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::MainSettingsWidget), isStart(false)
{
    qDebug() << "MAIN WINDOW CREATED\n";
    ui->setupUi(this);
    ui->degreeRadioButton->setChecked(true);
}

MainSettingsWidget::~MainSettingsWidget()
{
    if(ui)
    {
        delete ui;
    }
}

void MainSettingsWidget::setActive(bool isActive)
{
    QPushButton* button = ui->startStopButton;

    if(!isActive)
    {
        button->setText(tr("Start"));
        ui->xTranslationValue->setText("---");
        ui->yTranslationValue->setText("---");
        ui->zTranslationValue->setText("---");

        ui->xRotationValue->setText("---");
        ui->yRotationValue->setText("---");
        ui->zRotationValue->setText("---");
    }
    else
    {
        button->setText(tr("Stop"));
    }

    isStart = isActive;

}

void MainSettingsWidget::setXRotationValue(float value)
{
    ui->xRotationValue->setText(QString::number(value));
    ui->xRotationValue->update();
}

void MainSettingsWidget::setYRotationValue(float value)
{
    ui->yRotationValue->setText(QString::number(value));
}

void MainSettingsWidget::setZRotationValue(float value)
{
    ui->zRotationValue->setText(QString::number(value));
}

void MainSettingsWidget::setXTranslationValue(float value)
{
    ui->xTranslationValue->setText(QString::number(value));
}

void MainSettingsWidget::setYTranslationValue(float value)
{
    ui->yTranslationValue->setText(QString::number(value));
}

void MainSettingsWidget::setZTranslationValue(float value)
{
    ui->zTranslationValue->setText(QString::number(value));
}

void MainSettingsWidget::setRotationValue(const QQuaternion &value)
{
    QVector3D eulerAngles = value.toEulerAngles();
    if(ROTATION_UNIT::RADIANS == rotationUnit)
    {

        setXRotationValue(degreesToRadians(eulerAngles.x()));
        setYRotationValue(degreesToRadians(eulerAngles.y()));
        setZRotationValue(degreesToRadians(eulerAngles.z()));
    }
    else
    {
        setXRotationValue(eulerAngles.x());
        setYRotationValue(eulerAngles.y());
        setZRotationValue(eulerAngles.z());
    }
}

void MainSettingsWidget::setTranslationValue(const QVector3D &value)
{
    setXTranslationValue(value.x());
    setYTranslationValue(value.y());
    setZTranslationValue(value.z());
}


void MainSettingsWidget::on_zoomInButton_released()
{
    emit onZoomInButtonReleased();
}

void MainSettingsWidget::on_zoomOutButton_released()
{
    emit onZoomOutButtonReleased();
}

void MainSettingsWidget::on_resetViewButton_released()
{
    emit onResetButtonReleased();
}

void MainSettingsWidget::on_startStopButton_released()
{
    isStart = !isStart;
    emit onStartStopChanged(isStart);
}

void MainSettingsWidget::on_degreeRadioButton_clicked()
{
    rotationUnit = ROTATION_UNIT::DEGREES;
}

void MainSettingsWidget::on_radianRadioButton_clicked()
{
    rotationUnit = ROTATION_UNIT::RADIANS;
}

void MainSettingsWidget::on_calibrationButton_released()
{
    emit onCalibrationButtonReleased();
}
