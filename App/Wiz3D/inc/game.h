#ifndef GAME_H
#define GAME_H

/*!
 * \file
 * \brief Header file with Game class declaration.
 * Implemented game is simple game with starship.
 * The goal of the game is to avoid asteroids (minus points)
 * and gather positive points from stars.
 */

#include <QObject>
#include "collision_object.h"
#include <QList>
#include <QNode>
#include <QTimer>
#include <memory>

/*!
 * \class Game
 * \brief The Game class implements starship game
 */
class Game : public QObject
{
    Q_OBJECT
public:

    /*!
     * \brief Game class constructor
     * \param rootEntity - parent entity (parent scene)
     * \param parent - parent object
     */
    Game(Qt3DCore::QNode* rootEntity, QObject * parent = nullptr);

    /*!
     * \brief Game destructor
     */
    ~Game();

    /*!
     * \brief setMainObjectPosition - sets starship location
     * \param position - new starship location
     */
    void setMainObjectPosition(const QVector3D& position);

    /*!
     * \brief isActive method
     * \return true - if game is on
     * \return false - if game is off
     */
    bool isActive();

    /*!
     * \brief getPointsCounter - returns gathered positive points from stars
     * \return number of poinsts
     */
    int getPointsCounter() const;

    /*!
     * \brief getCollisionCounter - returns number of collisions with asteroids (minus points)
     * \return number of collisions
     */
    int getCollisionCounter() const;

public slots:
    /*!
     * \brief start the game
     */
    void start();
    /*!
     * \brief stop the game
     */
    void stop();

    /*!
     * \brief update game.
     * Move object and check if collision occured.
     */
    void update();

signals:
    /*!
     * \brief newCollision signal
     * Signal is emitted when collision with asteroid occured
     */
    void newCollision();
    /*!
     * \brief newPoint signal
     * Signal is emitted when star is gathered
     */
    void newPoint();

private:
    static constexpr int MAX_NUMBER_STARS = 10; //!< Max number of stars is scene at the time
    static constexpr int MAX_NUMBER_OBSTACLES = 20; //!< Max number of obstacles (asteroids) is scene at the time
    Qt3DCore::QNode* parentEntity; //!< Parent scene entity
    std::unique_ptr<CollisionObject> mainObject; //!< Starship entity
    std::unique_ptr<QList<CollisionObject*>> obstaclesList; //!< List of asteroids objects in the scene
    std::unique_ptr<QList<CollisionObject*>> pointsList; //!< List of stars in the scene
    std::unique_ptr<QTimer> timer; //!< Timer which emit signal with 10ms interval. Emitted signal causes update operation
    std::unique_ptr<std::uniform_int_distribution<int>> randGen; //!< Random number generator for objects location
    std::default_random_engine engine; //!< Numer generator engine
    int collisionCounter; //!< collision counter
    int pointsCounter; //!< scored poinst counter

    static constexpr float objSize = 2.0f;
};

#endif // GAME_H
