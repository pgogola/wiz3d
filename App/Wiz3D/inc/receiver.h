#ifndef RECEIVER_H
#define RECEIVER_H

/*!
 * \file
 * \brief Declaration of Receiver data class.
 *
 * This file constaint declaration of Transeiver class
 * which implements serial port receiver. Also class implements protocol
 * which parse arrived bytes.
 */



#include <QSerialPort>
#include <QObject>
#include <QQueue>
#include "sensor.h"
#include <fstream>
#include "container_thread_safe.h"

/*!
 * \class Receiver
 * \brief The Receiver class
 * Implements serial port receiver which receive data from UART
 */
class Receiver : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Receiver constructor
     * \param q - thread safe queue for received data
     * \param parent - parent object
     */
    Receiver(ContainerThreadSafe * q, QObject* parent = nullptr);
    /*!
     * \brief Receiver destructor
     */
    ~Receiver();
    /*!
     * \brief Function return serial port name used for connection
     * \return Serial Port Name (e.g: COM13)
     */
    const QString& getSerialPortName() const;

    /*!
     * \brief getIsConnected
     * \return true     - receiver is connected
     * \return false    - receiver is not connected
     */
    bool getIsConnected() const;

    /*!
     * \brief flushBuffer
     * Flush buffer with data
     */
    void flushBuffer();

public slots:
    void printData();
    void setSerialPortName(const QString &value);
    /*!
     * \brief Start serial port connection
     * Initialize receiving data from port. Serial port name needs to be set before.
     *
     * \return true if connection finished sucessful, false otherwise
     */
    bool startConnection();
    /*!
     * \brief Stop serial port connection.
     */
    void stopConnection();
    /*!
     * \brief Function which receive and parse received bytes into frame.
     */
    void receiveData();

signals:
    /*!
     * \brief Signal generated when new data from from sensor are received.
     */
    void newSensorData();

private slots:

private:
    QString serialPortName;     /*! Serial port name used for connection */
    QSerialPort serialPort;     /*! QSerialPort object witch implements connection */
    QByteArray* buffer;          /*! Buffor for received data */
    Sensor* sensor;              /*! Sensor object class. Data which are received and match to protocol
                                  are mapped into sensor and forwarded */
    bool isConnected;           /*! Is connection right now */
    ContainerThreadSafe* queue;

};

#endif // RECEIVER_H
