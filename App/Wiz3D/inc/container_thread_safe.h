#ifndef CONTAINER_THREAD_SAFE_H
#define CONTAINER_THREAD_SAFE_H

/*!
 * \file
 * \brief Thread safe FIFO queue for comunication between thread
 */

#include "sensor.h"

#include <QObject>
#include <QQueue>
#include <QMutex>

/*!
 * \class ContainerThreadSafe
 * \brief The ContainerThreadSafe class
 */
class ContainerThreadSafe
{
public:
    /*!
     * \brief ContainerThreadSafe constructor
     */
    ContainerThreadSafe();

    /*!
     * \brief Append Sensor data
     * \param sensor new sensor data
     */
    void enqueue(const Sensor& sensor);
    /*!
     * \brief Dequeue element from queue head
     * Remove and return data from queue head
     * \return Sensor data from queue's head
     */
    Sensor dequeue();

    /*!
     * \brief Get data from head without removing dettaching them
     * \return Reference to data in head of queue
     */
    Sensor& head();

    /*!
     * \brief Get data from head without removing dettaching them
     * \return Reference to data in head of queue
     */
    const Sensor& head() const;

    /*!
     * \brief isEmpty
     * \return true is empty
     * \return false is not empty
     */
    bool isEmpty() const;

    /*!
     * \brief length
     * \return length of queue
     */
    int length() const;



private:
    /*!
     * \brief mutex which is locked when any operation (enqueue/dequeue) occures
     * When mutex is locked, other operations are blocked
     */
    mutable QMutex mutex;

    /*!
     * \brief queue for sensor's data
     */
    QQueue<Sensor> queue;
};

#endif // CONTAINER_THREAD_SAFE_H
