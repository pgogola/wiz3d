#ifndef OBJECT_VIEW_H
#define OBJECT_VIEW_H
/*!
 * \file
 * \brief This file contains ObjectViewWidget class definition
 *  which implements main widget in application.
 */


#include <QWidget>
#include "object_view_scene.h"
#include "main_settings_widget.h"
namespace Ui {
class ObjectViewWidget;
}

/*!
 * \class ObjectViewWidget
 * \brief The ObjectViewWidget class
 *
 * Implements widget which is main part of application's main window.
 * This widget contains MainSettingWidget and GLObjectWidget.
 */
class ObjectViewWidget : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief ObjectViewWidget constructor
     * \param parent - parent object
     */
    explicit ObjectViewWidget(QWidget *parent = nullptr);
    /*!
     * \brief ObjectViewWidget destructor
     */
    ~ObjectViewWidget();
    /*!
     * \brief Return pointer to MainSettingsWidget inside ObjectViewWidget.
     * \return MainSettingWidget class.
     */
    MainSettingsWidget* getMainSettingsWidget() const;
    /*!
     * \brief Return pointer to GLObjectWidget inside ObjectViewWidget.
     * \return GLObjectWidget class.
     */
    ObjectViewScene *getGlObjectWidget() const;
public slots:


private:
    Ui::ObjectViewWidget *ui;
};

#endif // OBJECT_VIEW_H
