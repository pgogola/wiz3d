#ifndef COLLISION_OBJECT_H
#define COLLISION_OBJECT_H

/*!
 * \file
 * \brief Header file with CollisionObject class declaration.
 * CollisionObject class is renderable object.
 */



#include <QEntity>
#include <QNode>
#include <QObject>
#include <QVector3D>
#include "renderable_entity.h"

/*!
 * \class CollisionObject
 * \brief The CollisionObject class - renderable object with
 * collision detection feature. CollisionObject inherites
 * from RenderableObject
 */
class CollisionObject : public RenderableEntity
{
    Q_OBJECT
public:
    /*!
     * \brief CollisionObject constructor
     * \param scale - renderable object scale
     * \param location - initial object location
     * \param color - color of model texture
     * \param size - object size used in collision detection
     * \param parent - parent entity
     * \param modelURL - url path to obj model
     */
    CollisionObject(const float &scale,
                    const QVector3D& location,
                    const QColor& color,
                    const float size,
                    Qt3DCore::QNode *parent = nullptr,
                    const QUrl &modelURL = QUrl());
    /*!
     * \brief isCollision method. Compare location of two objects
     * and return information if collision occured
     * \param other - other CollisionObject to check if collision occured
     * \return true - if collision occured
     * \return false - if collision didn't occure
     */
    bool isCollision(const CollisionObject &other);

    /*!
     * \brief moveBy - moves object by given vector
     * \param movement - vector which defines movement
     */
    void moveBy(const QVector3D& movement);

    /*!
     * \brief setTranslation - set new object's location
     * \param tra - vector which defines new location
     */
    void setTranslation(const QVector3D &tra);

signals:
    /*!
     * \brief collisionOccured signal emited when collision occured
     */
    void collisionOccured();

private:
    float objSize; //!< Object size used in collision detection
};

#endif // COLLISION_OBJECT_H
