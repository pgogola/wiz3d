#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

/*!
 * \file
 * \brief Declaration of application's MainWindow class.
 *
 * This file contains declaration of application main window. Moreover MainWindow class
 * acts as controller in model-view-controller architecture used in this project.
 */
#include <QMainWindow>
#include "charts.h"
#include "object_model.h"
#include "object_view_widget.h"
namespace Ui {
class MainWindow;
}

/*!
 * \class MainWindow
 * \brief The MainWindow class
 *
 * Implements application main window and acts as a controler.
 * MainWindow manages all operations and processes between view and model.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief MainWindow constructor
     * \param parent - parent object
     */
    explicit MainWindow(QWidget *parent = nullptr);
    /*!
     * MainWindow destructor
     */
    ~MainWindow();

    /*!
     * \brief Prepare list of available COM ports.
     *
     * Add available COM ports to list in menu. Data from sensor is received from
     * serial port via COM port with Receiver class.
     */
    void menuAddAvailablePorts();

    /*!
     * \brief Initialize all necessary connections between ObjectViewWidget
     * (and its components - view) with ObjectModel (model).
     */
    void initializeConnectionsModelView();

    /*!
     * \brief Close main application window event
     */
    void closeEvent(QCloseEvent *event) override;
public slots:
    /*!
     * \brief Start or stop visualisation of object based on data from sensor.
     * \param isStart - if true start visualization, if false stop visualization.
     */
    void startStop(bool isStart);
    /*!
     * \brief Slot to capture COM por
     * t select event.
     * In this slot signal is emited to set COM port in Receiver object.
     * \param action - pointer to click event on selected COM port name.
     */
    void selectPortMenuOptions(QAction* action);
    /*!
     * \brief Slot which capture event to open window with charts showing sensor data
     * in real time.
     */
    void openChartWindow(bool);
    /*!
     * \brief Slot called when sensors from ObjectModel are calibrated and normal work
     * can start.
     */
    void calibrationFinished(ObjectModel::SENSOR_ID sensor);

    /*!
     * \brief Slot called when gyroscope calibration is finished
     * sensors from ObjectModel are calibrated and normal work can start.
     */
    void calibrationGyroFinished();

    /*!
     * \brief Slot called when accelerometer calibration is finished
     * sensors from ObjectModel are calibrated and normal work can start.
     */
    void calibrationAccFinished();

    /*!
     * \brief Slot called when calibration button was clicked and calibration starts.
     */
    void startCalibration();

private:
    Ui::MainWindow *ui;

    QMenuBar* menuBar;  //!< Menu bar

    ObjectViewWidget* objectViewWidget;     //!< Pointer to main widget in main window

    MainSettingsWidget* mainSettingsWidget; //!< Pointer to widget with settings in main window

    ObjectViewScene* objectViewScene3D;     //!< Pointer to 3D scene

    Charts* chartWindow;        //!< Pointer to window with graphs

    ObjectModel* objectModel;   //!< Pointer to model

    Receiver* receiver;         //!< Data from sensors receiver

    QThread receiverThread;     //!< Separated thread for receiver

    QThread objectModelThread;  //!< Separated thread for object model

    /*!
     * \brief Thread-safe queue for communication between receiver and objectModel threads
     */
    ContainerThreadSafe *queue;

    /*!
     * \brief accCalibrationOk
     * Flag for accelerometer calibration
     */
    bool accCalibrationOk;
    /*!
     * \brief gyroCalibrationOk
     * Flag for gyroscope calibration
     */
    bool gyroCalibrationOk;

    /*!
     * \brief warningDialog
     * Dialog used for example when calibration works
     */
    QMessageBox* warningDialog = nullptr;

};

#endif // MAIN_WINDOW_H
