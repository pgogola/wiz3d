#ifndef OBJECT_MODEL_H
#define OBJECT_MODEL_H

/*!
 * \file
 * \brief Definition of 3d object model (in model-view-controller).
 *
 * This file contains definiotion of ObjectModel class which implements
 * all necessary operation for view.
 *
 */


#include "receiver.h"
#include <QObject>
#include <QVector3D>
#include <memory>
#include "math.h"
#include <QMatrix4x4>
#include <limits>
#include "container_thread_safe.h"

/*!
 * \class ObjectModel
 * \brief The ObjectModel class
 *
 * Implements model of 3d object. ObjectModel class receive data from Receiver class
 * and processes them to get information about object orientation and position.
 * Also it converts raw into common used units such a degrees, meters/sec.
 *
 * When data in sensor object are arrived model checks if it is gyroscope or accelerometer sensor,
 * and then processes received data. Otherwise Sensor is not recognised.
 *
 */
class ObjectModel : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief The SENSOR_ID enum
     *
     * Gyroscope and accelerometer id used to recognise sensor
     */
    enum SENSOR_ID{
        GYROSCOPE = 1,      /*!< Gyroscope ID */
        ACCELEROMETER = 2   /*!< Accelerometer ID */
    };

    /*!
     * \brief Fuction converts angle in degrees into radians.
     * \param degrees - value in degrees to convert into radians.
     * \return angle in radians.
     */
    inline static float degreesToRadians(float degrees) {return degrees*M_PI/180.0;}

    /*!
     * \brief Fuction converts angle in radians into degrees.
     * \param radians - value in radians to convert into degrees.
     * \return angle in degrees.
     */
    inline static float radiansToDegrees(float radians) {return radians*180/M_PI;}

    /*!
     * \brief Function converts raw sensor acceleration into G units acceleration
     *
     * 1G ~= 9.81m/s^2
     *
     * \param rawAcceleration - raw acceleration value received from sensor
     * \return acceleration in G units.
     */
    inline static float rawToG(float rawAcceleration) {return rawAcceleration/RAW_PER_G_ACCELERATION;}

    /*!
     * \brief Function converts raw sensor acceleration into m/s^2 units acceleration
     * \param rawAcceleration - raw acceleration value received from sensor
     * \return acceleration in m/s^2 .
     */
    inline static float rawToMps(float rawAcceleration) {return rawAcceleration*EARTH_ACCELERATION/RAW_PER_G_ACCELERATION;}

    /*!
     * \brief ObjectModel constructor
     * \param q - thread safe queue
     * \param parent - parent class pointer
     */
    explicit ObjectModel(ContainerThreadSafe* q, QObject *parent = nullptr);

    /*!
     * \brief ObjectModel destructor
     */
    ~ObjectModel();

    static constexpr float GYROSCOPE_SENSITIV = 0.00875;    /*!< [dps/digit] Used Gyroscope sensitivity on FS = 250 dps */

    static constexpr int RAW_PER_G_ACCELERATION = INT16_MAX/2;  /*!< Raw value to G acceleration ratio */
    static constexpr float EARTH_ACCELERATION = 9.81;   /*!< Earth acceleration */

    /*!
     * \brief Reset object orientation.
     */
    void resetOrientation();

    /*!
     * \brief Reset object position.
     */
    void resetPosition();

    /*!
     * \brief Calculate rotation velocity from raw sensor data into degrees/sec
     * \param sensor - received sensor's data with raw values
     */
    void calculateRotationVelocity(const Sensor sensor);

    /*!
     * \brief  Calculate translation acceleration from raw sensor data into m/s^2
     * \param sensor - received sensor's data with raw values
     */
    void calculateTranslationAcceleration(const Sensor sensor);

    /*!
     * \brief Calculate orientation based on the data calculated in
     *  void calculateRotationVelocity(const Sensor &sensor)
     * and void calculateTranslationAcceleration(const Sensor &sensor)/
     */
    void calculateOrientation();

    /*!
     * \brief Calculate position based on the data calculated in
     *  calculateRotationVelocity and calculateTranslationAcceleration.
     */
    void calculatePosition();

    /*!
     * \brief Return orientation in quaternion representation.
     * \return quaternion representation of current orientation.
     */
    const QQuaternion &getOrientation() const;

    /*!
     * \brief Set current object orientation.
     * \param value
     */
    void setOrientation(const QQuaternion &value);

    /*!
     * \brief Get current object position.
     * \return current object position as vector.
     */
    const QVector3D& getPosition() const;

    /*!
     * \brief Set current object position.
     * \param value - new object position.
     */
    void setPosition(const QVector3D &value);

    /*!
     * \brief Start receiving sensor data from Receiver object.
     */
    void startReceive();
    /*!
     * \brief Stop receiving sensor data from Receiver object.
     */
    void stopReceive();

    /*!
     * \brief Get current rotation velocity in degrees/sec.
     * \return current rotation velocity.
     */
    const QVector3D& getRotationVelocity() const;

    /*!
     * \brief Set current roration velocity.
     * \param value - new rotation velocity in degrees/sec.
     */
    void setRotationVelocity(const QVector3D &value);

    /*!
     * \brief Get current translation acceleration in m/s^2.
     * \return current translation acceleration.
     */
    const QVector3D& getTranslationAcceleration() const;
    /*!
     * \brief Set current tranlation acceleration.
     * \param value - new translation acceleration in m/s^2.
     */
    void setTranslationAcceleration(const QVector3D &value);

    /*!
     * \brief Get current translation velocity in m/s.
     * \return current translation velocity.
     */
    const QVector3D& getTranslationVelocity() const;
    /*!
     * \brief Set current tranlation velocity.
     * \param value - new translation velocity in m/s.
     */
    void setTranslationVelocity(const QVector3D &value);

signals:
    /*!
     * \brief Signal emitted when object's position is changed.
     * \param Current position in x, y and z axes.
     */
    void positionChanged(const QVector3D&);
    /*!
     * \brief Signal emitted when object's orientation is changed.
     * \param Current orientation in quaternion representation.
     */
    void orientationChanged(const QQuaternion&);
    /*!
     * \brief Signal emitted when object's translation acceleration is changed.
     * \param Current translation acceleration in x, y and z axes.
     */
    void translationAccelerationChanged(const QVector3D&);
    /*!
     * \brief Signal emitted when object's rotation velocity is changed.
     * \param Current rotation velocity in x, y and z axes.
     */
    void rotationVelocityChanged(const QVector3D&);

    /*!
     * \brief Signal emitted when callibration of sensors is finished.
     */
    void calibrationComplitted(SENSOR_ID sensorId);

    /*!
     * \brief Signal emitted when gyroscope callibration finished.
     */
    void calibrationGyroComplitted();

    /*!
     * \brief Signal emitted when accelerometer callibration is finished.
     */
    void calibrationAccComplitted();

public slots:
    /*!
     * \brief Slot for sensor's data during normal work.
     */
    void receiveNewData();//const Sensor sensor);

    /*!
     * \brief Slot for sensor's data during calibarion.
     */
    void sensorsCalibration();

private:
    /*!
     * \brief Function which processes data from sensor to calibrate gyroscope.
     * \param sensor - new sensor's value.
     */
    void gyroscopeCalibration(const Sensor sensor);

    /*!
     * \brief Function which processes data from sensor to calibrate accelerometer.
     * \param sensor - new sensor's value.
     */
    void accelerometerCalibration(const Sensor sensor);

    /*!
     * \addtogroup Calibration
     * \brief Error values generated by sensors used to calibrate sensors.
     * @{
     */
    int xGyroscopeMax;  //!< Gyroscope x axis max self gain.
    int xGyroscopeMin;  //!< Gyroscope x axis min self gain.
    int xGyroscopeMean; //!< Gyroscope x axis mean self gain.
    int yGyroscopeMax;  //!< Gyroscope y axis max self gain.
    int yGyroscopeMin;  //!< Gyroscope y axis min self gain.
    int yGyroscopeMean; //!< Gyroscope y axis mean self gain.
    int zGyroscopeMax;  //!< Gyroscope z axis max self gain.
    int zGyroscopeMin;  //!< Gyroscope z axis min self gain.
    int zGyroscopeMean; //!< Gyroscope z axis mean self gain.

    int xAccelerometerMax;  //!< Accelerometer x axis max self gain.
    int xAccelerometerMin;  //!< Accelerometer x axis min self gain.
    int xAccelerometerMean; //!< Accelerometer x axis mean self gain.
    int yAccelerometerMax;  //!< Accelerometer y axis max self gain.
    int yAccelerometerMin;  //!< Accelerometer y axis min self gain.
    int yAccelerometerMean; //!< Accelerometer y axis mean self gain.
    int zAccelerometerMax;  //!< Accelerometer z axis max self gain.
    int zAccelerometerMin;  //!< Accelerometer z axis min self gain.
    int zAccelerometerMean; //!< Accelerometer z axis mean self gain.
    /*!
     * @}
     */
    QVector3D rotationVelocity;         //!< Current object's rotation velocity
    QVector3D translationAcceleration;  //!< Current object's translation acceleration

    QQuaternion orientation;            //!< Current object's orientation
    QVector3D position;                 //!< Current object's position

    QVector3D translationVelocity;      //!< Current object's translation velocity

    ContainerThreadSafe *queue;     //!< Thread safe queue for sensor data
};

#endif // OBJECT_MODEL_H
