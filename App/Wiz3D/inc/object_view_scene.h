#ifndef OBJECT_VIEW_SCENE_H
#define OBJECT_VIEW_SCENE_H


/*!
 * \file
 * \brief 3D scene built with Qt3D mechanisms
 */

#include "manipulator.h"
#include "renderable_entity.h"
#include "collision_object.h"
#include "game.h"
#include <QCamera>
#include <QDiffuseMapMaterial>
#include <QMaterial>
#include <QFirstPersonCameraController>
#include <QQuaternion>
#include <QTextureImage>
#include <QVector3D>
#include <QWidget>
#include <QCheckBox>
#include <QPushButton>
#include <QLabel>
#include <Qt3DCore/QTransform>

/*!
 * \class ObjectViewScene
 * \brief The ObjectViewScene class definition
 * Class defines 3D scene
 */
class ObjectViewScene : public QWidget
{
    Q_OBJECT
public:
    /*!
     * \brief ObjectViewScene constructor
     * \param parent - parent object
     */
    explicit ObjectViewScene(QWidget *parent = nullptr);
    /*!
     * \enum VisualizationMode
     * \brief The VisualizationMode enum
     */
    enum class VisualizationMode {
        OBJECT_VISUALIZATION, //!< Show object loaded from obj file
        MANIPULATOR, //!< Show manipulator object
        GAME //!< Game mode
    };

    /*!
     * \brief Set model
     * \param source - Url with path to *.obj source file
     */
    void setObjectModelSource(QUrl source);

    /*!
     * \brief Set object texture
     * \param source - Url with path to texture file
     */
    void setObjectTextureSource(QUrl source);

public slots:
    /*!
     * \brief Set object orientation.
     * Behaviours depends on VisualizationMode.
     * If mode is OBJECT_VISUALIZATION then whole object is rotated,
     * if mode is MANIPULATOR, then orientation controls joints' angles
     * \param orientation - current orientation received from sensor
     */
    void setOrientation(const QQuaternion &orientation);
    /*!
     * \brief Set object position. If VisualizationMode is MANIPULATOR, then no position is changed.
     * \param value - current position received from sensor
     */
    void setPosition(const QVector3D& value);
    /*!
     * \brief Set new absolute camera position.
     * \param value - vector describing new camera position
     */
    void setCamera(const QVector3D& value);
    /*!
     * \brief Set new relatice camera position.
     * \param value - vector describing movement of camera to new position.
     */
    void setCameraBy(const QVector3D& value);

    /*!
     * \brief Set visualization mode.
     * \param mode - new visualization mode. Allowed values are:
     * VisualizationMode::OBJECT_VISUALIZATION or VisualizationMode::MANIPULATOR
     */
    void setVisualizationMode(VisualizationMode mode);

private:
    QQuaternion orientation; //!< Current object orientation

    QVector3D position; //!< Current object position

    Qt3DCore::QEntity *rootEntity; //!< 3D scene main entity

    Qt3DRender::QCamera *cameraEntity;  //!< Camera entity

    Qt3DExtras::QFirstPersonCameraController *camController; //!< Camera controller

    Manipulator* manip; //!< Manipulator object

    RenderableEntity* objectEntity; //!< Renderable object loaded from obj file

    Game* game; //!< Game mode

    Qt3DRender::QTextureImage* objectDiffuseImage; //!< Renderable object texture

    Qt3DExtras::QDiffuseMapMaterial* diffuseMapMaterial; //!<

    VisualizationMode visualizationMode;    //!< Current visualization mode

    QUrl objectModelEntityUrl; //!< Path to *.obj file with model

    QUrl objectTextureUrl; //!< Path to file with texture

    QCheckBox* moveCheckBox;

    void createRenderableObject(); //!< Function which draw renderable object

    /*!
     * \brief Inline function to show or hide game button
     * and labels with collisions or points scored number.
     * \param show - if false then widgets will be hide
     * \param show - if true then widgets will be shown
     */
    inline void showGameStatus(bool show)
    {
        if(show)
        {
            gameButton->show();
            redPointsLabel->show();
            greenPointsLabel->show();
        }
        else
        {
            gameButton->hide();
            redPointsLabel->hide();
            greenPointsLabel->hide();
        }
    }

    QPushButton* gameButton;    //!< Game start/stop button
    QLabel* redPointsLabel;     //!< Label with collisions number
    QLabel* greenPointsLabel;   //!< Label with points scored number

    QMetaObject::Connection gameConnection; //!< Meta object for connection between Game object and start stop push button
};

#endif // OBJECT_VIEW_SCENE_H
