#ifndef RENDERABLEENTITY_H
#define RENDERABLEENTITY_H

/*!
 * \file
 * \brief File contains declaration of renderable 3d entity which can be loaded from obj file.
 */

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DRender/QMesh>

/*!
 * \class RenderableEntity
 * \brief The RenderableEntity class defines 3D object
 */
class RenderableEntity : public Qt3DCore::QEntity
{
public:
    /*!
     * \brief RenderableEntity class contructor
     * \param parent - parent entity
     */
    RenderableEntity(Qt3DCore::QNode *parent = nullptr);

    /*!
     * \brief Get object mesh
     * \return object mesh
     */
    Qt3DRender::QMesh *mesh() const;

    /*!
     * \brief Get object transform
     * \return object transform
     */
    Qt3DCore::QTransform *transform() const;

private:
    Qt3DRender::QMesh *m_mesh;  //!< Object mesh
    Qt3DCore::QTransform *m_transform; //!< Object transform
};

#endif // RENDERABLEENTITY_H
