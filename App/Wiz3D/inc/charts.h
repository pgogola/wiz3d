#ifndef CHARTS_H
#define CHARTS_H

#include <QMainWindow>

/*!
 * \file
 * \brief Declaration of Charts class.
 *
 * This file constaint declaration of Chart drawing class.
 */


#include "qcustomplot.h"


namespace Ui {
class Charts;
}


/*!
 * \class Charts
 * \brief The Charts class
 * Implements class which plot charts showing current data readed from gyroscope and accelerometer.
 * Each axis is plotted in separated simple chart using QCustomPlot library.
 */

class Charts : public QMainWindow
{
    Q_OBJECT


public:
    /*!
     * \brief Charts constructor
     * \param parent - parent object
     */
    explicit Charts(QWidget *parent = nullptr);

    /*!
     * \brief Charts destructor
     */
    ~Charts();

    /*!
     * \brief On close event
     * Overriding closeEvent funtion, shows QMessageBox with question
     * to exit
     */
    virtual void closeEvent(QCloseEvent *event) override;

public slots:
    /*!
     * \brief Slot called when new rotation value occured.
     * \param rotation - vector with rotation velocity values around x, y and z axes.
     */
    void newRotation(const QVector3D& rotation);
    /*!
     * \brief Slot called when new translation value occured.
     * \param translation - vector with rotation velocity values along x, y and z axes.
     */
    void newTranslation(const QVector3D& translation);


private:
    /*!
     * \brief Initialize charts.
     *
     * Initializes queues and maximum and minimum values shown in charts.
     */
    void initializeCharts();

    /*!
     * \brief Initialize simple chart with specific parameters.
     * \param chart - pointer to QCustomPlot object which shows specific data.
     * \param xLabel - x axis label
     * \param yLabel - y axis label
     * \param xRange - x maximum range
     * \param yRange - y maximum range
     */
    void initializeChart(QCustomPlot* chart, const QString& xLabel,
                         const QString& yLabel, QPointF xRange, QPointF yRange);
    /*!
     * \brief Add x and y value to chart and repaint plot.
     * \param plot - pointer to QCustomPlot object which shown specific data.
     * \param x - new x value - timestamp
     * \param y - new y value
     */
    void addAndReplot(QCustomPlot* plot, const double x, const double y);


    Ui::Charts *ui;

    static constexpr int MAX_Y_AXIS_ROT_VAL = 100;  //!< Maximum value in Y axis for rotation
    static constexpr int MAX_Y_AXIS_ACC_VAL = 1;    //!< Maximum value in Y axis for acceleration
    static constexpr int MAX_TIME_PERIOD = 20;      //!< Time window

};

#endif // CHARTS_H
