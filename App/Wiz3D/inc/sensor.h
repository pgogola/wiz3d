#ifndef SENSOR_H
#define SENSOR_H

/*!
 * \file
 * \brief Declaration of Sensor class. Which identity sensor and holds readed values.
 */
#include <QtGlobal>

/*!
 * \class Sensor
 * \brief Implements sensor object with 3 axes value x, y, z and sensorID
 */
class Sensor
{
public:
    /*!
     * \brief Sensor contructor
     */
    Sensor();

    /*!
     * \brief Returns x value
     * \return x value from sensor as quint16
     */
    qint16 getX() const;

    /*!
     * \brief Set x sensor value
     * \param value - new x value
     */
    void setX(const qint16 &value);

    /*!
     * \brief Returns y value
     * \return y value from sensor as quint16
     */
    qint16 getY() const;

    /*!
     * \brief Set y sensor value
     * \param value - new y value
     */
    void setY(const qint16 &value);

    /*!
     * \brief Returns z value
     * \return z value from sensor as quint16
     */
    qint16 getZ() const;

    /*!
     * \brief Set z sensor value
     * \param value - new z value
     */
    void setZ(const qint16 &value);

    quint16 getSensorID() const;

private:
    quint16 sensorID;   /*! Sensor id from x, y, z values are */
    qint16 x;           /*! x axis value */
    qint16 y;           /*! y axis value */
    qint16 z;           /*! z axis value */
};

#endif // SENSOR_H
