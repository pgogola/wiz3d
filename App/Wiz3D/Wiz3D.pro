#-------------------------------------------------
#
# Project created by QtCreator 2018-03-16T21:29:49
#
#-------------------------------------------------

QT       += core gui serialport charts opengl xml printsupport
QT       += 3dcore 3drender 3dinput 3dextras

CONFIG += c++11
#CONFIG += debug

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Wiz3D
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    ./inc/  \
    ./src/  \
    ./ui/   \

SOURCES += \
    src/crc16.cpp \
    src/main.cpp \
    src/main_settings_widget.cpp \
    src/main_window.cpp \
    src/object_model.cpp \
    src/object_view_widget.cpp \
    src/qcustomplot.cpp \
    src/sensor.cpp \
    src/receiver.cpp \
    src/container_thread_safe.cpp \
    src/charts.cpp \
    src/object_view_scene.cpp \
    src/manipulator.cpp \
    src/renderable_entity.cpp \
    src/collision_object.cpp \
    src/game.cpp

HEADERS += \
    inc/crc16.h \
    inc/main_settings_widget.h \
    inc/main_window.h \
    inc/object_model.h \
    inc/object_view_widget.h \
    inc/qcustomplot.h \
    inc/sensor.h \
    inc/receiver.h \
    inc/container_thread_safe.h \
    inc/charts.h \
    inc/object_view_scene.h \
    inc/manipulator.h \
    inc/renderable_entity.h \
    inc/collision_object.h \
    inc/game.h

FORMS += \
    ui/main_window.ui \
    ui/main_settings_widget.ui \
    ui/object_view_widget.ui \
    ui/charts.ui

RESOURCES += \
    resources/3dobjects.qrc

LIBS += -lOpengl32


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../QGLViewer/ -lQGLViewer2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../QGLViewer/ -lQGLViewerd2

INCLUDEPATH += $$PWD/../../../../../../QGLViewer
DEPENDPATH += $$PWD/../../../../../../QGLViewer

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../QGLViewer/libQGLViewer2.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../QGLViewer/libQGLViewerd2.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../QGLViewer/QGLViewer2.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../QGLViewer/QGLViewerd2.lib
