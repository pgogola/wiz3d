\contentsline {section}{\numberline {1}Opis projektu}{2}
\contentsline {section}{\numberline {2}Plan pracy}{2}
\contentsline {subsection}{\numberline {2.1}Harmonogram}{2}
\contentsline {subsection}{\numberline {2.2}Kamienie milowe}{2}
\contentsline {subsection}{\numberline {2.3}Wykres Gantta}{3}
\contentsline {section}{\numberline {3}Projekt graficznego interfejsu u\IeC {\.z}ytkownika}{3}
\contentsline {subsection}{\numberline {3.1}Wymagane funkcjonalno\IeC {\'s}ci}{3}
\contentsline {subsection}{\numberline {3.2}Projekt interfejsu}{4}
\contentsline {section}{\numberline {4}Komunikacja komputera z p\IeC {\l }ytk\IeC {\k a} rozwojow\IeC {\k a}, protok\IeC {\'o}\IeC {\l } komunikacyjny}{4}
\contentsline {section}{\numberline {5}Architektura programu}{5}
\contentsline {subsection}{\numberline {5.1}Diagram klas}{6}
\contentsline {section}{\numberline {6}Przyk\IeC {\l }adowe dzia\IeC {\l }anie programu}{7}
\contentsline {section}{\numberline {7}Opis element\IeC {\'o}w programu}{9}
\contentsline {subsection}{\numberline {7.1}Otrzymywanie i przetwarzanie danych z sensor\IeC {\'o}w}{9}
\contentsline {subsection}{\numberline {7.2}Usuwanie dryftu czujnik\IeC {\'o}w}{9}
\contentsline {subsection}{\numberline {7.3}Orientacja obiektu}{9}
\contentsline {subsection}{\numberline {7.4}Pozycja obiektu}{9}
\contentsline {subsection}{\numberline {7.5}Okno wizualizacji 3D}{9}
\contentsline {subsection}{\numberline {7.6}Okno wykres\IeC {\'o}w}{9}
\contentsline {section}{\numberline {8}Podsumowanie}{10}
