/*
 * gyro.h
 *
 *  Created on: 13.03.2018
 *      Author: Piotr Gogola
 */

#ifndef INC_GYROSCOPE_H_
#define INC_GYROSCOPE_H_


#include <stdint.h>
#include "../stm32l4xx_hal.h"

typedef struct {
	int16_t xAxisRotation;
	int16_t yAxisRotation;
	int16_t zAxisRotation;
} L3GD20_Rotation_t;

void L3GD20_Init();

void L3GD20_SelectChipHigh();

void L3GD20_SelectChipLow();

uint8_t L3GD20_ReadReg8(uint8_t address);

void L3GD20_WriteReg8(uint8_t address, uint8_t data);

uint16_t L3GD20_ReadReg16(uint8_t address);

void L3GD20_WriteReg16(uint8_t address, uint16_t data);

void L3GD20_ReadRotation(L3GD20_Rotation_t* rotation);

#endif /* INC_GYRO_H_ */
