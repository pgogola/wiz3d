/*
 * gyro.c
 *
 *  Created on: 13.03.2018
 *      Author: Piotr Gogola
 */

#include "gyroscope.h"

extern SPI_HandleTypeDef hspi2;

#define WHO_AM_I	0x0F

#define CTRL_REG1	0x20
#define CTRL_REG2	0x21
#define CTRL_REG3	0x22
#define CTRL_REG4	0x23
#define CTRL_REG5	0x24

#define REFERENCE	0x25
#define OUT_TEMP	0x26
#define STATUS_REG	0x27

#define OUT_X_L		0x28
#define OUT_X_H		0x29
#define OUT_Y_L		0x2A
#define OUT_Y_H		0x2B
#define OUT_Z_L		0x2C
#define OUT_Z_H		0x2D

#define FIFO_CTRL_REG	0x2E
#define FIFO_SRC_REG	0x2F

#define INT1_CFG	0x30
#define INT1_SRC	0x31

#define INT1_THS_XL		0x32
#define INT1_THS_XH		0x33
#define INT1_THS_YL		0x34
#define INT1_THS_YH		0x35
#define INT1_THS_ZL		0x36
#define INT1_THS_ZH		0x37

#define INT1_DURATION	0x38

#define SINGLE_READ		0x80
#define MULTIPLE_READ	0xC0

#define SINGLE_WRITE	0x00
#define MULTIPLE_WRITE	0x40

#define SBL(x)	(1<<x)

/**
 * CTRL_REG1 bits
 */
#define DR1		SBL(7)
#define DR0 	SBL(6)
#define BW1		SBL(5)
#define BW0		SBL(4)
#define PD		SBL(3)
#define Zen		SBL(2)
#define Xen		SBL(1)
#define Yen		SBL(0)

/**
 * CTRL_REG2 bits
 */
#define HPM1 	SBL(5)
#define HPM0 	SBL(4)
#define HPCF3 	SBL(3)
#define HPCF2 	SBL(2)
#define HPCF1	SBL(1)
#define HPCF0	SBL(0)

/**
 * CTRL_REG3 bits
 */
#define I1_Int1 	SBL(7)
#define I1_Boot 	SBL(6)
#define H_Lactive 	SBL(5)
#define PP_OD 		SBL(4)
#define I2_DRDY 	SBL(3)
#define I2_WTM 		SBL(2)
#define I2_ORun 	SBL(1)
#define I2_Empty	SBL(0)

/**
 * CTRL_REG4 bits
 */
#define BDU		SBL(7)
#define BLE 	SBL(6)
#define FS1 	SBL(5)
#define FS0		SBL(4)
#define SIM		SBL(0)

/**
 * CTRL_REG5 bits
 */
#define BOOT 		SBL(7)
#define FIFO_EN 	SBL(6)
#define HPen 		SBL(4)
#define INT1_Sel1 	SBL(3)
#define INT1_Sel0 	SBL(2)
#define Out_Sel1 	SBL(1)
#define Out_Sel0	SBL(0)

/**
 * STATUS_REG bits
 */
#define ZYXOR 	SBL(7)
#define ZOR 	SBL(6)
#define YOR  	SBL(5)
#define XOR  	SBL(4)
#define ZYXDA  	SBL(3)
#define ZDA  	SBL(2)
#define YDA  	SBL(1)
#define XDA 	SBL(0)

/**
 * FIFO_CTRL_REG bits
 */
#define FM2		SBL(7)
#define FM1		SBL(6)
#define FM0		SBL(5)
#define WTM4	SBL(4)
#define WTM3	SBL(3)
#define WTM2	SBL(2)
#define WTM1	SBL(1)
#define WTM0	SBL(0)

/**
 * FIFO_SRC_REG bits
 */
#define WTM 	SBL(7)
#define OVRN 	SBL(6)
#define EMPTY 	SBL(5)
#define FSS4 	SBL(4)
#define FSS3	SBL(3)
#define FSS2 	SBL(2)
#define FSS1 	SBL(1)
#define FSS0	SBL(0)

/**
 * INT1_CFG bits
 */
#define AND_OR	SBL(7)
#define LIR		SBL(6)
#define ZHIE	SBL(5)
#define ZLIE	SBL(4)
#define YHIE	SBL(3)
#define YLIE	SBL(2)
#define XHIE	SBL(1)
#define XLIE	SBL(0)

/**
 * INT1_SRC bits
 */
#define IA		SBL(6)
#define ZH		SBL(5)
#define ZL		SBL(4)
#define YH		SBL(3)
#define YL		SBL(2)
#define XH		SBL(1)
#define XL		SBL(0)

/**
 * INT1_THS_XH bits
 */
#define THSX14 	SBL(6)
#define THSX13 	SBL(5)
#define THSX12 	SBL(4)
#define THSX11 	SBL(3)
#define THSX10  SBL(2)
#define THSX9 	SBL(1)
#define THSX8 	SBL(0)

/**
 * INT1_THS_XL bits
 */
#define THSX7 	SBL(7)
#define THSX6 	SBL(6)
#define THSX5 	SBL(5)
#define THSX4 	SBL(4)
#define THSX3 	SBL(3)
#define THSX2   SBL(2)
#define THSX1 	SBL(1)
#define THSX0 	SBL(0)

/**
 * INT1_THS_YH bits
 */
#define THSY14 	SBL(6)
#define THSY13 	SBL(5)
#define THSY12 	SBL(4)
#define THSY11 	SBL(3)
#define THSY10  SBL(2)
#define THSY9 	SBL(1)
#define THSY8 	SBL(0)

/**
 * INT1_THS_YL bits
 */
#define THSY7 	SBL(7)
#define THSY6 	SBL(6)
#define THSY5 	SBL(5)
#define THSY4 	SBL(4)
#define THSY3 	SBL(3)
#define THSY2   SBL(2)
#define THSY1 	SBL(1)
#define THSY0 	SBL(0)

/**
 * INT1_THS_ZH bits
 */
#define THSZ14 	SBL(6)
#define THSZ13 	SBL(5)
#define THSZ12 	SBL(4)
#define THSZ11 	SBL(3)
#define THSZ10  SBL(2)
#define THSZ9 	SBL(1)
#define THSZ8 	SBL(0)

/**
 * INT1_THS_ZL bits
 */
#define THSZ7 	SBL(7)
#define THSZ6 	SBL(6)
#define THSZ5 	SBL(5)
#define THSZ4 	SBL(4)
#define THSZ3 	SBL(3)
#define THSZ2   SBL(2)
#define THSZ1 	SBL(1)
#define THSZ0 	SBL(0)

/**
 * INT1_DURATION bits
 */
#define D7 	SBL(7)
#define D6 	SBL(6)
#define D5 	SBL(5)
#define D4 	SBL(4)
#define D3 	SBL(3)
#define D2  SBL(2)
#define D1 	SBL(1)
#define D0 	SBL(0)

#define __SPI_DIRECTION_2LINES(__HANDLE__)   do{\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             }while(0);

#define __SPI_DIRECTION_2LINES_RXONLY(__HANDLE__)   do{\
                                                   CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                                   SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY);\
                                                   }while(0);

#define __SPI_DIRECTION_1LINE_TX(__HANDLE__) do{\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             }while(0);

#define __SPI_DIRECTION_1LINE_RX(__HANDLE__) do {\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_BIDIMODE);\
                                             } while(0);

void L3GD20_Init() {
//	1. Write CTRL_REG2
//	2. Write CTRL_REG3
//	3. Write CTRL_REG4
//	4. Write CTRL_REG6
//	5. Write REFERENCE
//	6. Write INT1_THS
//	7. Write INT1_DUR
//	8. Write INT1_CFG
//	9. Write CTRL_REG5
//	10. Write CTRL_REG1

	L3GD20_SelectChipHigh();
	HAL_Delay(100);

	uint8_t tmpreg;

	/* Read CTRL_REG5 register */
	tmpreg = L3GD20_ReadReg8(CTRL_REG5);

	/* Enable or Disable the reboot memory */
	tmpreg |= BOOT;

	/* Write value to MEMS CTRL_REG5 register */
	L3GD20_WriteReg8(CTRL_REG5, tmpreg);

	//register CTRL_REG2 configuration
	//high pass mode selection and cut off frequency
	L3GD20_WriteReg8(CTRL_REG2, HPM1 | HPCF1 | HPCF2);

	//register CTRL_REG3 configuration
//	L3GD20_WriteReg8(CTRL_REG3, HPM1 | HPCF1 | HPCF2);

//	register CTRL_REG4 configuration
//	L3GD20_WriteReg8(CTRL_REG4, BDU/* | FS0 | FS1*/);

	//register CTRL_REG6 configuration
//	L3GD20_WriteReg8(CTRL_REG5, HPen);

	//register CTRL_REG6 configuration
//	L3GD20_WriteReg8(CTRL_REG6, HPM1 | HPCF1 | HPCF2);

	//register REFERENCE configuration
//	L3GD20_WriteReg8(REFERENCE, HPM1 | HPCF1 | HPCF2);

	//register CTRL_REG4 configuration
//	L3GD20_WriteReg8(INT1_THS_, HPM1 | HPCF1 | HPCF2);

	//register CTRL_REG6 configuration
//	L3GD20_WriteReg8(CTRL_REG6, HPM1 | HPCF1 | HPCF2);

	//register REFERENCE configuration
//	L3GD20_WriteReg8(REFERENCE, HPM1 | HPCF1 | HPCF2);

//	/*
//		 * Normal mode of sleep
//		 * X axis enable
//		 * Y axis enable
//		 * Z axis enable
//		 * ODR == 190Hz
//		 * Cut-Off == 50
//		 */
	L3GD20_WriteReg8(CTRL_REG1, PD | Zen | Xen | Yen);
	HAL_Delay(200);
//
//

//	L3GD20_WriteReg8(CTRL_REG4, FS0 | BDU);
//	/*
//	 * Little endian data selecion
//	 */
////	L3GD20_WriteReg8(CTRL_REG4, BLE);
//	HAL_Delay(100);

}

void L3GD20_SelectChipHigh() {
	HAL_GPIO_WritePin(MEMS_GYRO_CS_GPIO_Port, MEMS_GYRO_CS_Pin, GPIO_PIN_SET);
}

void L3GD20_SelectChipLow() {
	HAL_GPIO_WritePin(MEMS_GYRO_CS_GPIO_Port, MEMS_GYRO_CS_Pin, GPIO_PIN_RESET);
}

uint8_t L3GD20_ReadReg8(uint8_t address) {
	L3GD20_SelectChipLow();
	uint8_t receive = 0;
	address |= SINGLE_READ;
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
#if UART_DEBUG
	printf("ReadReg8\r\nL3GD20 Send address register status = %d\r\n", (int)HAL_SPI_GetState(&hspi2));
#endif
	HAL_SPI_Receive(&hspi2, &receive, 1, 10);
#if UART_DEBUG
	printf("ReadReg8\r\nL3GD20 Receive data register status = %d\r\n", (int)HAL_SPI_GetState(&hspi2));
	printf("Received data = %d\r\n", (int)receive);
#endif
	L3GD20_SelectChipHigh();
	return receive;
}

void L3GD20_WriteReg8(uint8_t address, uint8_t data) {
	L3GD20_SelectChipLow();
	address |= SINGLE_WRITE;
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
#if UART_DEBUG
	printf("Write reg\r\nL3GD20 Send address register status = %d\r\n", (int)HAL_SPI_GetState(&hspi2));
#endif
	HAL_SPI_Transmit(&hspi2, &data, 1, 10);
#if UART_DEBUG
	printf("Write reg\r\nL3GD20 Transmit data register status = %d\r\n", (int)HAL_SPI_GetState(&hspi2));
#endif
	L3GD20_SelectChipHigh();
}

uint16_t L3GD20_ReadReg16(uint8_t address) {
	uint16_t receive = 0;
	L3GD20_SelectChipLow();
	address |= MULTIPLE_READ;
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
	HAL_SPI_Receive(&hspi2, (uint8_t*) &receive, 2, 10);
	if (!(address & SBL(3))) {
		asm("REV16 %1,%0"
				: "=r" (receive)
				: "r" (receive));
	}
	L3GD20_SelectChipHigh();
	return receive;
}

void L3GD20_WriteReg16(uint8_t address, uint16_t data) {
	uint8_t dat[] = { address | MULTIPLE_WRITE, (uint8_t) (data >> 8),
			(uint8_t) data };
	L3GD20_SelectChipLow();
	__SPI_DIRECTION_2LINES(&hspi2);
	HAL_SPI_Transmit(&hspi2, dat, 3, 10);
	L3GD20_SelectChipHigh();
}

void L3GD20_ReadRotation(L3GD20_Rotation_t* rotation) {

	while (!(L3GD20_ReadReg8(STATUS_REG) & ZYXDA)) {
	}
//	while(!(L3GD20_ReadReg8(STATUS_REG) & (1<<7))) {
//	}
	rotation->xAxisRotation = L3GD20_ReadReg16(OUT_X_L);
	rotation->yAxisRotation = L3GD20_ReadReg16(OUT_Y_L);
	rotation->zAxisRotation = L3GD20_ReadReg16(OUT_Z_L);

}
