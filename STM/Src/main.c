/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "gyroscope.h"
#include "accelerometer.h"
#include <string.h>
#include <stdlib.h>
#include "checksum.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

int _write(int file, char* ptr, int len) {
	HAL_UART_Transmit(&huart2, (uint8_t*) ptr, len, 100);
	return len;
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
typedef struct {
	uint16_t sendorID;
	int16_t x;
	int16_t y;
	int16_t z;
} Sensor;

typedef struct {
	uint8_t start;
	uint8_t len;
	uint8_t* payload;
	uint16_t crc;
	uint8_t end;

} Frame;

void spiChange(uint32_t direction) {
	__HAL_SPI_DISABLE(&hspi2);
	hspi2.Init.Direction = direction;
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void) {
	/* USER CODE BEGIN 1 */
	L3GD20_Rotation_t rotation;
	LSM303_Acceleration_t acceleration;

	Sensor gyroscope;
	Sensor accelerometer;
	gyroscope.sendorID = 1;
	accelerometer.sendorID = 2;

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_SPI2_Init();
	MX_USART2_UART_Init();
	/* USER CODE BEGIN 2 */
	spiChange(SPI_DIRECTION_1LINE);
	LSM303_Init();
	LSM303_ReadReg8(0x0f);
	spiChange(SPI_DIRECTION_2LINES);
	L3GD20_Init();
//	while(1)
//	{
	printf("%d\r\n", L3GD20_ReadReg8(0x0f));
//	}
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

	while (1) {
		/**
		 * Gyroscope - read data from sensor and send via UART
		 */
		spiChange(SPI_DIRECTION_2LINES);
		L3GD20_ReadRotation(&rotation);
		gyroscope.x = rotation.xAxisRotation;
		gyroscope.z = -rotation.yAxisRotation;
		gyroscope.y = rotation.zAxisRotation;
//		printf("Gx = %d\r\n", rotation.xAxisRotation);
//		printf("Gy = %d\r\n", rotation.yAxisRotation);
//		printf("Gz = %d\r\n", rotation.zAxisRotation);
		uint8_t length = sizeof(gyroscope) + 5;
		uint8_t* buffer = (uint8_t*) malloc(length);
		memcpy(buffer + 2, &gyroscope, sizeof(gyroscope));
		buffer[0] = 0xaa;
		buffer[1] = length;
		buffer[length - 1] = 0x55;
		uint16_t crc = crc_16(buffer + 1, length - 4);
		buffer[length - 3] = crc >> 8;
		buffer[length - 2] = crc;
		HAL_UART_Transmit(&huart2, buffer, length, 10);
		free(buffer);
		HAL_Delay(10);
		/**
		 * Accelerometer - read data from sensor and send via UART
		 */
//		uint8_t length ;
//		uint8_t* buffer;
//		int16_t crc;
		spiChange(SPI_DIRECTION_1LINE);
		LSM303_ReadAcceleration(&acceleration);
		accelerometer.x = acceleration.xAxisAcceleration;
		accelerometer.z = acceleration.yAxisAcceleration;
		accelerometer.y = -acceleration.zAxisAcceleration;
//		printf("A x = %d\r\n", acceleration.xAxisAcceleration);
//		printf("A y = %d\r\n", acceleration.yAxisAcceleration);
//		printf("A z = %d\r\n", acceleration.zAxisAcceleration);
		/*uint8_t */length = sizeof(accelerometer) + 5;
		/*uint8_t **/buffer = (uint8_t*) malloc(length);
		memcpy(buffer + 2, &accelerometer, sizeof(gyroscope));
		buffer[0] = 0xaa;
		buffer[1] = length;
		buffer[length - 1] = 0x55;
		/*uint16_t */crc = crc_16(buffer + 1, length - 4);
		buffer[length - 3] = crc >> 8;
		buffer[length - 2] = crc;
		HAL_UART_Transmit(&huart2, buffer, length, 10);
		free(buffer);
//
		HAL_Delay(10);
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.MSICalibrationValue = 0;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 40;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
