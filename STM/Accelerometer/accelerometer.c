/*
 * lsm303.c
 *
 *  Created on: 30.04.2018
 *      Author: Piotr Gogola
 */

#include "accelerometer.h"
#define UART_DEBUG 0
extern SPI_HandleTypeDef hspi2;

#define WHO_AM_I_A		0x0f

#define ACT_THS_A		0x1e
#define ACT_DUR_A		0x1f

#define CTRL_REG1_A		0x20
#define CTRL_REG2_A		0x21
#define CTRL_REG3_A		0x22
#define CTRL_REG4_A		0x23
#define CTRL_REG5_A		0x24
#define CTRL_REG6_A		0x25
#define CTRL_REG7_A		0x26

#define STATUS_REG_A	0x27

#define OUT_X_L_A		0x28
#define OUT_X_H_A		0x29
#define OUT_Y_L_A		0x2a
#define OUT_Y_H_A		0x2b
#define OUT_Z_L_A		0x2c
#define OUT_Z_H_A		0x2d

#define FIFO_CTRL		0x2e
#define FIFO_SRC		0x2f
#define IG_CFG1_A		0x30
#define IG_SRC1_A		0x31
#define IG_THS_X1_A		0x32
#define IG_THS_Y1_A		0x33
#define IG_THS_Z1_A		0x34
#define IG_DUR1_A		0x35
#define IG_CFG2_A		0x36
#define IG_SRC2_A		0x37
#define IG_THS2_A		0x38
#define IG_DUR2_A		0x39

#define XL_REFERENCE	0x3a
#define XH_REFERENCE	0x3b
#define YL_REFERENCE	0x3c
#define YH_REFERENCE	0x3d
#define ZL_REFERENCE	0x3e
#define ZH_REFERENCE	0x3f

#define SINGLE_WRITE 	0x00
#define SINGLE_READ		0x80

#define SBL(x)	(1<<x)

/**
 * ACT_THS_A
 */
#define THS6	SBL(6)
#define THS5 	SBL(5)
#define THS4 	SBL(4)
#define THS3 	SBL(3)
#define THS2 	SBL(2)
#define THS1 	SBL(1)
#define THS0	SBL(0)

/**
 * ACT_DUR_A
 */
#define DUR7 	SBL(7)
#define DUR6 	SBL(6)
#define DUR5 	SBL(5)
#define DUR4 	SBL(4)
#define DUR3 	SBL(3)
#define DUR2 	SBL(2)
#define DUR1 	SBL(1)
#define DUR0	SBL(0)


/**
 * CTRL_REG1_A
 */
#define HR		SBL(7)
#define ODR2 	SBL(6)
#define ODR1 	SBL(5)
#define ODR0 	SBL(4)
#define BDU 	SBL(3)
#define ZEN 	SBL(2)
#define YEN 	SBL(1)
#define XEN		SBL(0)



/**
 * STATUS_REG_A
 */
#define ZYXOR 	SBL(7)
#define ZOR 	SBL(6)
#define YOR 	SBL(5)
#define XOR 	SBL(4)
#define ZYXDA 	SBL(3)
#define ZDA 	SBL(2)
#define YDA 	SBL(1)
#define XDA		SBL(0)


#define __SPI_DIRECTION_2LINES(__HANDLE__)   do{\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             }while(0);

#define __SPI_DIRECTION_2LINES_RXONLY(__HANDLE__)   do{\
                                                   CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                                   SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY);\
                                                   }while(0);

#define __SPI_DIRECTION_1LINE_TX(__HANDLE__) do{\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             }while(0);

#define __SPI_DIRECTION_1LINE_RX(__HANDLE__) do {\
                                             CLEAR_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_RXONLY | SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE);\
                                             SET_BIT((__HANDLE__)->Instance->CR1, SPI_CR1_BIDIMODE);\
                                             } while(0);

void LSM303_Init() {
	LSM303_SelectChipHigh();
	HAL_Delay(100);

	LSM303_WriteReg8(CTRL_REG4_A, 0x07);
	LSM303_WriteReg8(CTRL_REG2_A, /*1<<6 | */1 << 3 | 1 << 2 );
	LSM303_WriteReg8(CTRL_REG1_A, /*HR | */ODR1 | ODR0 | XEN | YEN | ZEN | BDU);
	HAL_Delay(100);
}

void LSM303_SelectChipHigh() {
	HAL_GPIO_WritePin(ACC_CS_GPIO_Port, ACC_CS_Pin, GPIO_PIN_SET);
}

void LSM303_SelectChipLow() {
	HAL_GPIO_WritePin(ACC_CS_GPIO_Port, ACC_CS_Pin, GPIO_PIN_RESET);
}

uint8_t LSM303_ReadReg8(uint8_t address) {
	LSM303_SelectChipLow();

	uint8_t receive = 0;
	address |= SINGLE_READ;
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
#if UART_DEBUG
	printf("ReadReg8\r\nL3GD20 Send address register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
#endif
	__SPI_DIRECTION_1LINE_RX(&hspi2);
	HAL_SPI_Receive(&hspi2, &receive, 1, 10);
#if UART_DEBUG
	printf("ReadReg8\r\nL3GD20 Receive data register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
	printf("Received data = %d\r\n", (int) receive);
#endif
	LSM303_SelectChipHigh();
	return receive;
}

void LSM303_WriteReg8(uint8_t address, uint8_t data) {
	LSM303_SelectChipLow();
	address |= SINGLE_WRITE;
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
#if UART_DEBUG
	printf("Write reg\r\nLSM303 Send address register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
#endif
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &data, 1, 10);
#if UART_DEBUG
	printf("Write reg\r\nLSM303 Transmit data register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
#endif
	LSM303_SelectChipHigh();
}

uint16_t LSM303_ReadReg16(uint8_t address) {
	uint16_t receive = 0;
	LSM303_SelectChipLow();
	address |= SINGLE_READ;
	__SPI_DIRECTION_1LINE_TX(&hspi2);
	HAL_SPI_Transmit(&hspi2, &address, 1, 10);
#if UART_DEBUG
	printf("Write reg\r\nL3GD20 Send address register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
#endif
	__SPI_DIRECTION_1LINE_RX(&hspi2);
	HAL_SPI_Receive(&hspi2, (uint8_t*) &receive, 2, 10);
//	asm("REV16 %1,%0"
//					: "=r" (receive)
//					: "r" (receive));
#if UART_DEBUG
	printf("Write reg\r\nL3GD20 Transmit data register status = %d\r\n",
			(int) HAL_SPI_GetState(&hspi2));
#endif
	LSM303_SelectChipHigh();
	return receive;
}

//void LSM303_WriteReg16(uint8_t address, uint16_t data);

void LSM303_ReadAcceleration(LSM303_Acceleration_t* acc) {
	while (!(LSM303_ReadReg8(STATUS_REG_A) & ZYXDA)) {
	}
	acc->xAxisAcceleration = LSM303_ReadReg16(OUT_X_L_A);
	acc->yAxisAcceleration = LSM303_ReadReg16(OUT_Y_L_A);
	acc->zAxisAcceleration = LSM303_ReadReg16(OUT_Z_L_A);
}

