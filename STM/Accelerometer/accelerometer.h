/*
 * lsm303.h
 *
 *  Created on: 30.04.2018
 *      Author: Piotr Gogola
 */

#ifndef ACCELEROMETER_H_
#define ACCELEROMETER_H_

#include <stdint.h>
#include "../stm32l4xx_hal.h"

/**
 * Accelerometer registers definitions
 */

typedef struct {
	int16_t xAxisAcceleration;
	int16_t yAxisAcceleration;
	int16_t zAxisAcceleration;
} LSM303_Acceleration_t;

void LSM303_Init();

void LSM303_SelectChipHigh();

void LSM303_SelectChipLow();

uint8_t LSM303_ReadReg8(uint8_t address);

void LSM303_WriteReg8(uint8_t address, uint8_t data);

uint16_t LSM303_ReadReg16(uint8_t address);

//void LSM303_WriteReg16(uint8_t address, uint16_t data);

void LSM303_ReadAcceleration(LSM303_Acceleration_t* acc);


#endif /* ACCELEROMETER_H_ */
